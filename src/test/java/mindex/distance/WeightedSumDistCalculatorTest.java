/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mindex.distance;

import java.util.Arrays;
import java.util.Random;
import mindex.MIndexObjectInitilizer;
import mindex.distance.WeightedSumDistCalculatorCorrected;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author xnovak8
 */
public class WeightedSumDistCalculatorTest {
    
    public WeightedSumDistCalculatorTest() {
    }

    /**
     * Test of getQueryDistance method, of class WeightedSumDistCalculator.
     */
    //@Test
    public void testGetQueryDistance() {
        System.out.println("getQueryDistance");
        short[] ppp = null;
        WeightedSumDistCalculatorCorrected instance = null;
        float expResult = 0.0F;
        float result = instance.getQueryDistance(ppp);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    private Random random = new Random(System.currentTimeMillis());
    
    /**
     * Test of getPartialQueryDistance method, of class WeightedSumDistCalculator.
     */
    @Test
    public void testGetPartialQueryDistance() {
        System.out.println("getPartialQueryDistance");
        for (short maxPivots = 16; maxPivots <= 1024; maxPivots *= 2) {
            short pppLength = 8;
            float [] queryPivotDistances = new float [maxPivots];
            for (int i = 0; i < queryPivotDistances.length; i++) {
                queryPivotDistances[i] = random.nextFloat();
            }
    
            short [] queryPPP = MIndexObjectInitilizer.sortDistances(queryPivotDistances, pppLength);
            WeightedSumDistCalculatorCorrected calculator = new WeightedSumDistCalculatorCorrected(pppLength, queryPivotDistances, queryPPP);
//            WeightedSumDistCalculatorPure pureCalculator = new WeightedSumDistCalculatorPure(pppLength, queryPivotDistances, queryPPP);
            WeightedSumDistCalculatorTight tightCalculator = new WeightedSumDistCalculatorTight(pppLength, queryPivotDistances, queryPPP);
            
            StringBuilder stringBuilder = new StringBuilder();
            for (int x = 0; x < 10; x++) {
                short [] ppp = new short[pppLength];
                for (int i = 0; i < ppp.length; i++) {
                    ppp[i] = (short) random.nextInt(maxPivots);
                }
                if (x == 0) {
                    ppp = queryPPP;
                }
                
                float fullDistance = calculator.getQueryDistance(ppp);
                //float fullPureDistance = pureCalculator.getQueryDistance(ppp);
                float fullTigthDistance = tightCalculator.getQueryDistance(ppp);
                stringBuilder.append("distance estimations from level 0 to " + pppLength +": ");
                StringBuilder pureStrB = new StringBuilder("\tpure sum distance estimations: ");
                StringBuilder oldStrB = new StringBuilder("\toriginal sum distance estimations: ");
                StringBuilder tightStrB = new StringBuilder("\ttight sum distance estimations: ");
                
                float sumOfDiffs = 0f;
                float sumOfDiffsOld = 0f;
                float sumOfDiffsPure = 0f;
                float sumOfDiffsTight = 0f;
                for (int midLevel = 1; midLevel <= pppLength + 1; midLevel++) {
                    float topDistance = calculator.getQueryDistance(new short [0]) + 
                            calculator.getPartialQueryDistance(Arrays.copyOf(ppp, midLevel - 1), 1);
                    stringBuilder.append(topDistance).append(", ");
                    sumOfDiffs += fullDistance - topDistance;
                    
//                    float pureDist = pureCalculator.getPartialQueryDistance(Arrays.copyOf(ppp, midLevel - 1), 1);
//                    pureStrB.append(pureDist).append(", ");
//                    sumOfDiffsPure += fullPureDistance - pureDist;
//                    
                    float tightDist = tightCalculator.getQueryDistance(Arrays.copyOf(ppp, midLevel - 1));
                    tightStrB.append(tightDist).append(", ");
                    sumOfDiffsTight += fullTigthDistance - tightDist;
                    
                    short [] lowerPPP = new short [pppLength - midLevel + 1];
                    System.arraycopy(ppp, midLevel - 1, lowerPPP, 0, pppLength - midLevel + 1);
                    float lowerDistance = calculator.getPartialQueryDistance(lowerPPP, midLevel);
                    assertEquals(fullDistance, topDistance + lowerDistance, 0.00001d);
                }
                stringBuilder.append(": ").append(sumOfDiffs / (pppLength + 1)).append("\n")
                        .append(tightStrB).append(": ").append(sumOfDiffsTight / (pppLength + 1)).append("\n")
                        .append(oldStrB).append(": ").append(sumOfDiffsOld / (pppLength + 1)).append("\n")
                        .append(pureStrB).append(": ").append(sumOfDiffsPure / (pppLength + 1)).append("\n");
//                assertEquals(fullDistance, pureCalculator.getQueryDistance(ppp), 0.00001d);
            }
            System.out.println(stringBuilder.toString());
        }
    }    

    /**
     * Test of getCellDistanceEstimation method, of class WeightedSumDistCalculator.
     */
    //@Test
    public void testGetCellDistanceEstimation() {
        System.out.println("getCellDistanceEstimation");
        short[] ppp = null;
        WeightedSumDistCalculatorCorrected instance = null;
        float expResult = 0.0F;
        float result = instance.getCellDistanceEstimation(ppp);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}