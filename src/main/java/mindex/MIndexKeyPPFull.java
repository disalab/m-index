/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import messif.objects.nio.BinaryInput;
import messif.objects.nio.BinaryOutput;
import messif.objects.nio.BinarySerializable;
import messif.objects.nio.BinarySerializator;

     
/**
 * M-Index object key that contains only the pivot permutation prefix.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class MIndexKeyPPFull extends MIndexPPSimple implements MIndexKey, BinarySerializable {

    /** Class serial id for serialization. */
    private static final long serialVersionUID = 721101L;

    /** The decimal part of the key - the normalized distance between and object and its pivot. */
    protected float normalizedDistance;

    
    public MIndexKeyPPFull(short[] pivotPermutation, short maxLevel, short nPivots, float normalizedDistance, short level) {
        super(pivotPermutation, maxLevel, nPivots);
        this.normalizedDistance = normalizedDistance;
        getClusterNumber(level);
    }
    
    /**
     * This method returns the number of cluster corresponding to this PP an level; it also depends on the maxlevel (size of PP).
     * @return M-Index number of corresponding cluster
     */
    @Override
    public long getClusterNumber() {
        return getClusterNumber(precomputedLevel);
    }

    @Override
    public float getNormalizedDistance() {
        return normalizedDistance;
    }
    

   /**
     * Compare the keys according to the cluster.distance value
     * @param o the key to compare this key with
     */
    @Override
    public int compareTo(MIndexKey o) {
        if (! (o instanceof MIndexKey)) {
            return 3;
        }
        long diff = getClusterNumber() - o.getClusterNumber();
        
        if (diff == 0l) {
            return Float.compare(getNormalizedDistance(), o.getNormalizedDistance());
        }
        return (diff < 0l) ? -1 : 1;
    }

    
    //************ Serializable and BinarySerializable interface ************//

    /**
     * Serialize object including precomputed cluster number and level.
     * @param out
     * @throws IOException 
     */
    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        out.writeShort(precomputedLevel);
        out.writeLong(clusterNumber);
    }
    
    /**
     * Deserialize object including precomputed cluster number and level.
     */
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        precomputedLevel = in.readShort();
        clusterNumber = in.readLong();
    }
    
    /**
     * Creates a new instance of MIndexFinalKey loaded from binary input stream.
     * 
     * @param input the stream to read the key from
     * @param serializator the serializator used to write objects
     * @throws IOException if there was an I/O error reading from the stream
     */
    protected MIndexKeyPPFull(BinaryInput input, BinarySerializator serializator) throws IOException {
        super(input, serializator);
        precomputedLevel = serializator.readShort(input);
        clusterNumber = serializator.readLong(input);
        normalizedDistance = serializator.readFloat(input);
    }

    /**
     * Binary-serialize this object into the <code>output</code>.
     * @param output the output stream this object is binary-serialized into
     * @param serializator the serializator used to write objects
     * @return the number of bytes actually written
     * @throws IOException if there was an I/O error during serialization
     */
    @Override
    public int binarySerialize(BinaryOutput output, BinarySerializator serializator) throws IOException {
        return super.binarySerialize(output, serializator) + serializator.write(output, precomputedLevel)
                + serializator.write(output, clusterNumber) + serializator.write(output, normalizedDistance);
    }

    /**
     * Returns the exact size of the binary-serialized version of this object in bytes.
     * @param serializator the serializator used to write objects
     * @return size of the binary-serialized version of this object
     */
    @Override
    public int getBinarySize(BinarySerializator serializator) {
        return super.getBinarySize(serializator) + serializator.getBinarySize(precomputedLevel) 
                + serializator.getBinarySize(clusterNumber) + serializator.getBinarySize(normalizedDistance);
    }

    @Override
    public String toString() {
        return (new StringBuilder()).append(Arrays.toString(pivotPermutation)).append(" ").append(Float.toString(getNormalizedDistance()).substring(1)).toString();
    }
    
    
}
