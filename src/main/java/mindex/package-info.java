
/**
 * <p>
 * This Java package contains a implementation of the M-Index similarity indexing structure as
 *  described in papers published by similarity group from Masaryk university, Brno, Czech Republic.
 *</p>
 * <ul>
 *   <li>David Novak, Michal Batko. Metric Index: An Efficient and Scalable Solution for Similarity Search.
 *   In Proceedings of the 2009 Second International Workshop on Similarity Search and Applications.
 *   Washington, DC, USA : IEEE Computer Society. 9 s, 2009.</li>
 *   <li>David Novak, Michal Batko, Pavel Zezula. (2011). Metric Index: An Efficient and Scalable Solution for Precise and
 * Approximate Similarity Search. Information Systems, 36(4), 721–733. DOI:10.1016/j.is.2010.10.002</li>
 * </ul>
 * <p>
 * If you use it for academic purposes, please put reference to this or some subsequent paper.
 *</p>
 * <p>
 * To compile and use this library, you need current version of the MESSIF library and
 *  the Skip-Graphs implementation by Masaryk University similarity team.
 *</p>
 * <p>
 *  The M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *</p>
 * <p>
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *</p>
 * <p>
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 * </p>
 */
package mindex;

