/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectList;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.operations.RankingSingleQueryOperation;
import mindex.algorithms.MultipleMIndexAlgorithm;
import mindex.distance.KendallTauDistanceCalculator;
import mindex.distance.QPDistDiffCalculatorDistNorm;
import mindex.distance.QueryPPPDistanceCalculator;
import mindex.distance.QueryPivotDistDiffCalculator;
import mindex.distance.WeightedSumDistCalculator;
import mindex.distance.WeightedSumDistCalculatorCorrected;
import mindex.distance.WeightedSumDistCalculatorTight;

/**
 * This is a set of auxiliary static utilities for M-Index-related operations.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class MetricIndexes {

    /** Logger. */
    public static final Logger logger = Logger.getLogger("M-Index");
    
    /** String of parameter that differentiates various Query-PPP distance calculators */
    public static String QUERY_PPP_DIST_CALC = "QUERY_PPP_DIST_CALC";

    /** String of parameter that differentiates various Query-PPP distance calculators */
    public static String MAX_LEVEL = "MAX_LEVEL";
    
    /** TODO: this method should be in future on "AbstractObject"
     * Given an object, this method simply returns its {@link MIndexObjectPivotPermutation}, if it exists, 
     *  null is returned otherwise.
     * @param object data object to read pivot permutation from
     * @return {@link MIndexObjectPivotPermutation}, if it exists, null is returned otherwise
     */
    public static MIndexPP readMIndexPP(LocalAbstractObject object) {
        MIndexPP objectPP = object.getDistanceFilter(MIndexPPSimple.class);
        if (objectPP == null) {
            objectPP = object.getDistanceFilter(MIndexKeyPPFull.class);
        }
        return objectPP;
    }
    
    /**
     * This method checks whether the cluster specified by given permutation can be filtered out using repetitive
     *  application of double-pivot distance constraint.
     * @param nodePP cluster specification - pivot combination
     * @param radius query radius to check the filtering for
     * @return true, if the cluster can be filtered out
     */
    public static boolean filterOutCluster(float[] queryPivotDistances, short[] queryPermutation, short[] nodePP, float radius) {
        if (radius == LocalAbstractObject.MAX_DISTANCE) {
            return false;
        }

        boolean filterOutCluster = false;
        // we always try to use as "shortest" distance to query as possible (queryLevelToConsider index)
        int queryLevelToConsider = 0;
        for (int i = 0; i < nodePP.length; i++) {
            // try to filter out the created cluster using the Double-Pivot Constraint
            if (queryPivotDistances[nodePP[i]] - queryPivotDistances[queryPermutation[queryLevelToConsider]] > 2f * radius) {
                filterOutCluster = true;
                //MetricIndex.logger.info("(level = "+ firstContext.level +") Filtering cluster: "+Arrays.toString(firstContext.pivotCombination)+": "+firstContext.pivotCombination[i]+" - "+queryIndexes[queryLevelToConsider]);
                break;
            }
            // we must give up using the "shortest" distance to query, because we are within a cluster
            // that already used this pivot as "the shortest" (on some level)
            if ((nodePP[i] == queryPermutation[queryLevelToConsider]) && (i + 1 < nodePP.length)) {
                // probably, we have to give up using more than one "shortest" distance
                boolean actualFilteringIndexInCombination;
                do {
                    //MetricIndex.logger.info("(level = "+ firstContext.level +") Increasing the index of query combination for: "+ Arrays.toString(firstContext.pivotCombination)+" to " +(queryLevelToConsider + 1) +": "+Arrays.toString(this.queryIndexes));
                    queryLevelToConsider++;
                    actualFilteringIndexInCombination = false;
                    for (int j = 0; j < i; j++) {
                        if (nodePP[j] == queryPermutation[queryLevelToConsider]) {
                            actualFilteringIndexInCombination = true;
                            break;
                        }
                    }
                } while (actualFilteringIndexInCombination);
            }
        }
        return filterOutCluster;
    }
    
    /**
     * Given a query operation (with a query object) and some additional information, 
     *  this method returns the required calculator of "distance" between the query
     *  and pivot permutation prefixes stored in the M-Index (or PPPCode index).
     *  
     */
    public static QueryPPPDistanceCalculator getQueryPPPDistCalculator(RankingSingleQueryOperation operation, MetricIndex mIndex) throws AlgorithmMethodException {
        QueryPPPDistanceCalculator retVal;
        switch ((String) operation.getParameter(QUERY_PPP_DIST_CALC, String.class, "")) {
            case "KendallTauDistanceCalculator":
                retVal = new KendallTauDistanceCalculator(operation.getQueryObject(), mIndex);
                break;
            case "WeightedSumDistCalculatorCorrected":
                retVal = new WeightedSumDistCalculatorCorrected(operation, mIndex);
                break;
            case "QueryPivotDistDiffCalculator":
                retVal = new QueryPivotDistDiffCalculator(operation.getQueryObject(), mIndex);
                break;
            case "QPDistDiffCalculatorDistNorm":
                retVal = new QPDistDiffCalculatorDistNorm(operation.getQueryObject(), mIndex);
                break;
            case "WeightedSumDistCalculator":
                retVal = new WeightedSumDistCalculator(operation, mIndex);
                break;
            case "WeightedSumDistCalculatorTight":
            default:
                retVal = new WeightedSumDistCalculatorTight(operation, mIndex);
        }
        retVal.setMaxLevel(Math.min(mIndex.getMaxLevel(), operation.getParameter(MAX_LEVEL, Integer.class, Integer.MAX_VALUE).intValue()));
        return retVal;
    }
    
    /**
     * Given a pivot file, number of partitions to divide this file to, and the index,
     *  this method reads the objects (pivots) from the file, divides it to {@link nPartitions} 
     *  (equally, the last part can be smaller) and returns a temporary file with the 
     *  {@link iPartition}-th part.
     * @param pivotFile source pivot file
     * @param objectClass class of the pivot objects
     * @param nPartitions number of partitions (overlays) to divide the pivot set into
     * @param iPartition index of the partition to return (0-based)
     * @return temporary file with string representations of the objects
     */
    public static String getIthPivotSet(String pivotFile, Class<LocalAbstractObject> objectClass, int nPartitions, int iPartition) {
        try {
            AbstractObjectList<LocalAbstractObject> allPivots = new AbstractObjectList<>(new StreamGenericAbstractObjectIterator<>(objectClass, pivotFile));
            int partSize = (int) Math.ceil(allPivots.size() / nPartitions);
            
            File tempFile = File.createTempFile("pivots-", ".data");
            try (BufferedOutputStream tempFileOS = new BufferedOutputStream(new FileOutputStream(tempFile))) {
                for (int i = partSize * iPartition; i < partSize * (iPartition + 1) && i < allPivots.size(); i++) {
                    allPivots.get(i).write(tempFileOS);
                }
            }
            
            return tempFile.getAbsolutePath();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
            return null;
        } 
    }

    /**
     * This auxiliary method partitions given pivot set to equally large parts and returns the i-th part 
     *  of it (in form of a temporary file). The temp batch is created in the same directory where
     *  the original pivot file is stored: dir/pivotfile_L&lt;L&gt;_i
     * @param allPivotFile file with all the pivots
     * @param objectClass data class of the pivots
     * @param L number of indexes to partition the pivots to (<code>Math.floor(n/L)</code>).
     * @param i 1-based index of batch of pivots to be printed into the temporary file that is returned
     * @return temporary file containing i-th batch of pivots 
     */
    public static String getPartialPivotFile(File allPivotFile, Class<? extends LocalAbstractObject> objectClass, int L, int i) {
        if (! allPivotFile.exists()) {
            throw new IllegalArgumentException("pivot file '" + allPivotFile + "' does not exist");
        }
        
        try (StreamGenericAbstractObjectIterator<LocalAbstractObject> iterator = 
                new StreamGenericAbstractObjectIterator<>(objectClass, allPivotFile.toString())) {
            
            // read all the pivots into a temporary collection of objects
            AbstractObjectList<LocalAbstractObject> allPivots = new AbstractObjectList<>(iterator);

            File file = new File(allPivotFile.getParentFile(), allPivotFile.getName() + "_L" + L + "_" + i);
            try (OutputStream output = new BufferedOutputStream(new FileOutputStream(file))) {
                int nPivots = allPivots.size() / L;            
                for (int j = nPivots * (i - 1); j < nPivots * i; j++) {
                    allPivots.get(j).write(output);
                }
            }
            
            return file.toString();
        } catch (IllegalArgumentException | IOException ex) {
            Logger.getLogger(MultipleMIndexAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
            
    /**
     * 
     * @param string
     * @return
     */
    public static int simpleArithmetic(String string) {
         String[] split = string.split("[\\*\\/+-]");
        if (split.length != 2) {
            return Integer.valueOf(string).intValue();
        }
        switch (string.charAt(split[0].length())) {
            case '*':
                return Integer.valueOf(split[0]) * Integer.valueOf(split[1]);
            case '/':
                return Integer.valueOf(split[0]) / Integer.valueOf(split[1]);
            case '+':
                return Integer.valueOf(split[0]) + Integer.valueOf(split[1]);
            case '-':
                return Integer.valueOf(split[0]) - Integer.valueOf(split[1]);
            default:
                return -1;
        }
    }

    /** 
     * Execute any 
     * @param path 
     */
    public static void runProcessWait(String path) {
        try {
            Runtime rt = Runtime.getRuntime();
            Process p = rt.exec(path);
            p.waitFor();
        } catch (IOException | InterruptedException exc) {/*handle exception*/
            logger.log(Level.SEVERE, "Error running '" + path + "'", exc);
        }
    }
       
    /**
     * Logs given message to standard log with level "INFO".
     * @param message message to log
     */
    public static void logInfoMessage(String message) {
        logger.info(message);
    }
}
