/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex;

/**
 * Super class of any configuration parameter of M-Index (or other system).
 *  It stores <ul>
 *  <li>property key</li>
 *  <li>value type</li>
 *  <li>default value</li>
 *  <li>comment text (can be null)</li>
 * </ul>
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public interface Parameter {
    
    /**
     * Type of the parameter.
     */
    public static enum Type {
        STRING,
        BOOL,
        INT,
        SHORT,
        LONG,
        OBJECT
    };
    
    /**
     * Returns the key string of this parameter.
     * @return key string of this parameter (e.g. in the property file)
     */
    public String getKey();

    /**
     * One of the type values from {@link Parameter#Type}.
     * @return type of this parameter (String, Integer, etc.)
     */
    public Type getType();

    /**
     * Default value, if the value is not set.
     * @return default string value used if the value is not set
     */
    public String getDefaultValue();
    
    /**
     * The comment string of this parameter (can be null)
     * @return the comment string (should start with "#")
     */
     public String getComment();
    
}
