/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.processors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import messif.algorithms.AlgorithmMethodException;
import messif.buckets.BucketErrorCode;
import messif.objects.LocalAbstractObject;
import messif.operations.data.DeleteOperation;
import mindex.MetricIndex;
import mindex.navigation.VoronoiLeafCell;

/**
 * Implementation of NavigationProcessor interface that can handle DeleteOperation
 * and is thread safe - the object is deleted only once even if more threads
 * call processNext simultaneously 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class DeleteOperationNavigationProcessor extends DataManipulationNavigationProcessor {
    
    /** List of not deleted objects. */
    private final Collection<LocalAbstractObject> notDeletedObjects = Collections.synchronizedCollection(new ArrayList<LocalAbstractObject>());
    
    /**
     * Create the delete processor from a delete operation and current M-Index settings.
     * @param operation
     * @param mIndex
     * @throws AlgorithmMethodException 
     */
    public DeleteOperationNavigationProcessor(DeleteOperation operation, MetricIndex mIndex) throws AlgorithmMethodException {
        super(false, operation, mIndex, operation.getObjectsToDelete());
        if (! operation.isCheckingLocator()) {
            throw new IllegalArgumentException("PPP-Codes can only delete objects based on locator");
        }
    }

    @Override
    protected void processObjectsOnLeaf(VoronoiLeafCell leaf, List<LocalAbstractObject> objects) throws AlgorithmMethodException {
        leaf.readLock();
        try {
            if (!leaf.isValid()) {
                for (LocalAbstractObject object : objects) {
                    addToCellObjectMap(object);
                }
                return;
            }
            notDeletedObjects.addAll(leaf.deleteObjects(objects, 0, true));
        } finally {
            leaf.readUnLock();
        }
    } 

    @Override
    public void close() {
        if (notDeletedObjects.isEmpty()) {
            operation.endOperation();
        } else {
            if (notDeletedObjects.size() == ((DeleteOperation) operation).getObjectsToDelete().size()) {
                operation.endOperation(BucketErrorCode.OBJECT_NOT_FOUND);
            } else {
                operation.endOperation(BucketErrorCode.SOME_OBJECT_NOT_FOUND);
                ((DeleteOperation) operation).setNotDeleted(notDeletedObjects);
            }
        }
        super.close();
    }
}
