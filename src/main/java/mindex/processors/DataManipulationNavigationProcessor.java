/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.processors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import messif.algorithms.AlgorithmMethodException;
import messif.algorithms.AsynchronousNavigationProcessor;
import messif.objects.LocalAbstractObject;
import messif.operations.AbstractOperation;
import messif.operations.data.BulkInsertOperation;
import messif.operations.data.DataManipulationOperation;
import messif.operations.data.DeleteOperation;
import messif.operations.data.InsertOperation;
import mindex.MetricIndex;
import mindex.navigation.VoronoiLeafCell;

/**
 * Implementation of NavigationProcessor interface that can handle both
 * InsertOperation and BulkInsertOperation.
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public abstract class DataManipulationNavigationProcessor implements AsynchronousNavigationProcessor<AbstractOperation> {

    /** Maps objects that should be inserted into the index to appropriate leaves */
    private final Map<VoronoiLeafCell, List<LocalAbstractObject>> cellObjectMap = new HashMap<>();   
    
    /** How many LeafCells were already processed (all objects that should be inserted into this leaf were inserted) */
    private int processed = 0;

    /** How many LeafCells were retrieved from the {@link #cellObjectMap} (used for counting # of running "operations") */
    private int retrievedFromQueue = 0;
    
    /** Either BulkInsertOperation or simple InsertOperation that will be processed by this processor */
    protected final DataManipulationOperation operation;
    
    /** MetricIndex (NavigationDirectory) into which the objects should be inserted */
    protected final MetricIndex mIndex;

    /** If true, the Voronoi tree path is created, if necessary (this is probably "insert" operation). */
    protected final boolean createPath;


    protected DataManipulationNavigationProcessor(boolean createPath, DataManipulationOperation operation, MetricIndex mIndex, Collection<? extends LocalAbstractObject> objects) throws AlgorithmMethodException {
        this.operation = operation;
        this.mIndex = mIndex;
        this.createPath = createPath;
        initializeObjects(objects);
    }

    /**
     * A process step of this Processor means inserting all LocalAbstractObjects
     * that are mapped to a MIndexLeafCell into its Bucket
     * @return true  if the are unprocessed StructureLeafNodes
     *         false otherwise
     */
    @Override
    public boolean isFinished() {
        return cellObjectMap.isEmpty();
    }

    /**
     * Returns the next processing item in the queue.
     * @return the next processing item in the queue or <tt>null</tt> if the queue is empty
     * @throws InterruptedException if the waiting for the next item in the queue has been interrupted
     */
    protected Entry<VoronoiLeafCell, List<LocalAbstractObject>> getNextProcessingItem() throws InterruptedException {
        synchronized (cellObjectMap) {
            while (cellObjectMap.isEmpty()) {
                if (processed >= retrievedFromQueue) {
                    return null;
                }
                cellObjectMap.wait();
            }
            Entry<VoronoiLeafCell, List<LocalAbstractObject>> objectsToBeUpdated = cellObjectMap.entrySet().iterator().next();
            cellObjectMap.remove(objectsToBeUpdated.getKey());
            cellObjectMap.notifyAll();
            retrievedFromQueue ++;
            return objectsToBeUpdated;
        }
    }
    
    /**
     * Processes next MIndexLeafCell and inserts LocalAbstractObjects that
     * are mapped to it into its Bucket.
     * 
     * @return the number of updated objects
     */
    @Override
    public boolean processStep() throws AlgorithmMethodException, InterruptedException {
        Entry<VoronoiLeafCell, List<LocalAbstractObject>> objectsToBeUpdated = getNextProcessingItem();
        if (objectsToBeUpdated == null) {
            return false;
        }
        // Insert the objects into the MIndexLeafCell
        try {
            processObjectsOnLeaf(objectsToBeUpdated.getKey(), objectsToBeUpdated.getValue());
        } finally {
            synchronized (cellObjectMap) {
                processed++;
                cellObjectMap.notifyAll();
            }
        }
        return true;
    }

    @Override
    public Callable processStepAsynchronously() throws InterruptedException {
        final Entry<VoronoiLeafCell, List<LocalAbstractObject>> objectsToBeUpdated = getNextProcessingItem();
        if (objectsToBeUpdated == null)
            return null;
        return new Callable<AbstractOperation>() {
            @Override
            public AbstractOperation call() throws InterruptedException, AlgorithmMethodException {
                try {
                    processObjectsOnLeaf(objectsToBeUpdated.getKey(), objectsToBeUpdated.getValue());
                } finally {
                    synchronized (cellObjectMap) {
                        processed++;
                        cellObjectMap.notifyAll();
                    }
                }
                return operation;
            }
        };
    }
    
    /**
     * Insert the given objects into the given leaf cell. If some of the objects
     * could NOT be inserted (i.e. bucket is full) split the leaf cell.
     * @param leaf
     * @param objects to be inserted
     * @throws messif.algorithms.AlgorithmMethodException
     */
    protected abstract void processObjectsOnLeaf(VoronoiLeafCell leaf, List<LocalAbstractObject> objects) throws AlgorithmMethodException;

    /**
     * A process step of this Processor means inserting all LocalAbstractObjects
     * that are mapped to a MIndexLeafCell into its Bucket
     * @return the number of LeafCells that were already processed
     */
    @Override
    public int getProcessedCount() {
        return processed;
    }

    /**
     * A process step of this Processor means inserting all LocalAbstractObjects
     * that are mapped to a MIndexLeafCell into its Bucket. After calling processNext()
     * method number of times this method returns hasProcessNext should still be called
     * as some more objects might need to be processed because of occurrence of a split.
     * @return the number of StructureLeafNodes that are to be processed
     */
    @Override
    public int getRemainingCount() {
        return cellObjectMap.size() - processed;
    }

    /**
     * If processing of the operation this NavigationProcessor holds is ended
     * prematurely - not all leaf nodes were processed - unlock the remaining
     * leaf nodes
     */
    @Override
    public void close() {
        for (VoronoiLeafCell leaf : cellObjectMap.keySet()) {
            leaf.readUnLock();
        }
        if (! operation.isFinished()) {
            operation.endOperation();
        }
    }

    @Override
    public AbstractOperation getOperation() {
        return this.operation;
    }

    /**
     * For the given INITIALIZED object (i.e. {@code MetricIndex.readMIndexPP(object)} != null) find the responsible leaf cell
     * add it to the global cell object map and if not already locked, acquire the read lock on the leaf cell
     * @param object to be added to the list for its responsible MIndexLeafCell
     * @throws AlgorithmMethodException 
     */
    protected void addToCellObjectMap(LocalAbstractObject object) throws AlgorithmMethodException {
        VoronoiLeafCell leaf = mIndex.getVoronoiCellTree().getResponsibleLeaf(mIndex.readPPP(object), createPath);
        if (leaf == null) {
            return;
        }
        
        synchronized (cellObjectMap) {
            if (cellObjectMap.containsKey(leaf)) {
                cellObjectMap.get(leaf).add(object);
            } else {
                List<LocalAbstractObject> objects = new ArrayList<>();
                objects.add(object);
                cellObjectMap.put(leaf, objects);
            }
            cellObjectMap.notifyAll();
        }
    }

    /**
     * Computes MIndexKey for each of the objects and ands it to the list of its responsible leaf cell.
     * If allowed - initializing the objects (computing distances to pivots) is done in parallel.
     * @param objects to be initialized
     * @throws AlgorithmMethodException if anything goes wrong
     */
    private void initializeObjects(Collection<? extends LocalAbstractObject> objects) throws AlgorithmMethodException {
        for (LocalAbstractObject object : mIndex.initPP(objects)) {
            addToCellObjectMap(object);
        }
    }

    /**
     * Factory method for creating an instance of InsertOperationNavigationProcessor for given Bulk/InsertOperation
     * If allowed - initializing the objects (computing distances to pivots) is done in parallel. Acquires write lock on the leaf nodes.
     * @param operation - either instance of InsertOperation or BulkInsertOperation
     * @param mIndex
     * @return initialized instance of InsertOperationNavigationProcessor
     * @throws AlgorithmMethodException 
     */
    public static DataManipulationNavigationProcessor getInstance(AbstractOperation operation, MetricIndex mIndex) throws AlgorithmMethodException {
        if (operation instanceof InsertOperation) {
            return new InsertOperationNavigationProcessor((InsertOperation) operation, mIndex);
        } else if (operation instanceof BulkInsertOperation) {
            return new InsertOperationNavigationProcessor((BulkInsertOperation) operation, mIndex);
        } else if (operation instanceof DeleteOperation) {
            return new DeleteOperationNavigationProcessor((DeleteOperation) operation, mIndex);
        } else {
            throw new UnsupportedOperationException("UpdateNavigationProcessor can be created only for InsertOperation, BulkInsertOperation or DeleteOperation");
        }
    }
}
