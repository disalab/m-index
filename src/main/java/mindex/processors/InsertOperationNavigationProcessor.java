/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.processors;

import java.util.Collections;
import java.util.List;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import messif.operations.data.BulkInsertOperation;
import messif.operations.data.InsertOperation;
import mindex.MetricIndex;
import mindex.navigation.SplitConfiguration;
import mindex.navigation.VoronoiLeafCell;

/**
 * Implementation of NavigationProcessor interface that can handle both
 * InsertOperation and BulkInsertOperation.
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class InsertOperationNavigationProcessor extends DataManipulationNavigationProcessor {

    /**
     * Inits the processor for a single-object insert operation. The object is initialized
     *  by M-Index (set M-Index pivot permutation) and added to the cell map.
     * @param operation single-object insert operation
     * @param mIndex M-Index object
     * @throws AlgorithmMethodException if the initialization goes wrong
     */
    public InsertOperationNavigationProcessor(InsertOperation operation, MetricIndex mIndex) throws AlgorithmMethodException {
        super(true, operation, mIndex, Collections.singletonList(operation.getInsertedObject()));
    }

    /**
     * Inits the processor for a multi-object bulk-insert operation. The objects are initialized
     *  by M-Index (set M-Index pivot permutation) and added to the cell map.
     * @param operation single-object insert operation
     * @param mIndex M-Index object
     * @throws AlgorithmMethodException if the initialization goes wrong
     */
    public InsertOperationNavigationProcessor(BulkInsertOperation operation, MetricIndex mIndex) throws AlgorithmMethodException {
        super(true, operation, mIndex, operation.getInsertedObjects());
    }
    
    /**
     * Insert the given objects into the given leaf cell. If some of the objects
     * could NOT be inserted (i.e. bucket is full) split the leaf cell.
     * @param leaf
     * @param objects to be inserted
     * @throws messif.algorithms.AlgorithmMethodException
     */
    @Override
    protected void processObjectsOnLeaf(VoronoiLeafCell leaf, List<LocalAbstractObject> objects) throws AlgorithmMethodException {
        leaf.readLock();
        SplitConfiguration splitConfig = null;
        try {
            if (!leaf.isValid()) {
                for (LocalAbstractObject object : objects) {
                    addToCellObjectMap(object);
                }
                return;
            }
            splitConfig = leaf.insertObjects(objects);
        } finally {
            leaf.readUnLock();
        }
        if (splitConfig != null) { // if any splitConfig is returned, split the leaf
            // some of the objects weren't inserted into the leaf cell's bucket
            // so split the leaf cell, insert them into appropriate leaf cells.
            leaf.writeLock();
            try {
                leaf.split(splitConfig);
            } finally {
                leaf.writeUnLock();
            }
            
            // upper layers calling this processor need to handle proper insertion of objects after split by calling processStep() method
            for (LocalAbstractObject object : splitConfig.getToReinsert()) {
                addToCellObjectMap(object);
            }
        }
    }

}
