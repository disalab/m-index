/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.processors;

import java.io.Serializable;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import messif.buckets.impl.AlgorithmStorageBucket;
import messif.operations.Approximate;
import messif.operations.Approximate.LocalSearchType;
import messif.operations.query.ApproxKNNQueryOperation;
import messif.statistics.OperationStatistics;
import mindex.MetricIndex;
import mindex.MetricIndexes;
import mindex.algorithms.MIndexAlgorithm;
import mindex.navigation.MIndexLeafCell;
import mindex.navigation.VoronoiLeafCell;

/**
 * Auxiliary class bearing actual information about various types of search costs (in a given cluster, part of index or
 *  the whole index). It is mainly meant for approximate query processing.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class ApproxSearchCosts implements Serializable, Cloneable {

    /** Class id for serialization. */
    private static final long serialVersionUID = 51402L;

    // *****************************    Static settings for the I/O costs   ********************** //
    /** Size of disk data block */
    protected final static int blockSize = (int) Math.pow(2, 12);

    /** Object disk size for current data class in bytes */
    protected static int objectDiskSize = blockSize;

    /** Average number of objects per a data block. */
    protected static float objectsPerBlock = 1;

    protected final static float avgBTreeOccupation = 0.83f;
    
    protected static float objectsPerBTreeNode = 1;

    
    // ******************************    Static methods for I/O statistics   **************************** //

    public static void setObjectDiskSize(int actualObjectDiskSize) {
        if ((objectDiskSize != blockSize) && (objectDiskSize != actualObjectDiskSize)) {
            MetricIndexes.logger.warning("object disk size is set to a different value than before: BEWARE that only indexes of one data type can run within one process (TODO)");
        }
        objectDiskSize = actualObjectDiskSize;
        objectsPerBlock = ((float) blockSize) / ((float) actualObjectDiskSize);
        objectsPerBTreeNode = (float) (((float) (blockSize / actualObjectDiskSize)) * avgBTreeOccupation);
    }

    public static int getObjectDiskSize() {
        return objectDiskSize;
    }
    
    public static float getObjectsPerBlock() {
        return objectsPerBlock;
    }

    public static float getObjectsPerBTreeNode() {
        return objectsPerBTreeNode;
    }    
    
    /**
     * Given a number of objects read from the disk, this method returns the number of disk blocks read from the disk 
     *  (according to current setting - size of objects and size of disk block).
     * @param accessedObjects number of objects read from the disk
     * @return number of disk blocks read during accessing the objects (calculated)
     */
    public static int blockReadsForObjects(int accessedObjects) {
        return (int) Math.ceil(accessedObjects / objectsPerBlock);
    }

    /**
     * Given a number of disk blocks read from the disk, this method returns the maximal number objects that can
     * be stored in a disk space occupied by these blocks
     *  (according to current setting - size of objects and size of disk block).
     * @param blockReads number of blocks to be read from the disk
     * @return max number of objects stored in a given number of disk blocks
     */
    public static int objectsForBlockReads(int blockReads) {
        return (int) Math.floor(blockReads * objectsPerBlock);
    }
    
    /**
     * Given a number of objects read from the disk, this method returns the number of disk blocks read from the disk 
     *  (according to current setting - size of objects and size of disk block).
     * @param accessedObjects number of objects read from the disk
     * @return number of disk blocks read during accessing the objects (calculated)
     */
    public static int blockReadsForObjectsBTree(int accessedObjects) {
        return (int) Math.ceil(accessedObjects / objectsPerBTreeNode);
    }
    
    /**
     * Actual values of various types of search costs.
     */
    protected Map<LocalSearchType, AtomicInteger> costs = new EnumMap<>(LocalSearchType.class);

    /** Value of the local approximation parameter, whose interpretation depends on the value of {@link #localSearchType}. */
    protected int approxSearchParam = -1;

    /** Type of the local approximation parameter used. */
    protected LocalSearchType approxSearchType = null;

    // ****************  Getters and setters  ************************** //
    public int getApproxSearchParam() {
        return approxSearchParam;
    }

    public LocalSearchType getApproxSearchType() {
        return approxSearchType;
    }

 
    /** 
     * Constructor with given approximation parameter and its type. If the type of the parameter is {@link LocalSearchType#PERCENTAGE}
     *  then it is converted to {@link LocalSearchType#ABS_OBJ_COUNT} using the third parameter (total number of objects in the 'partition').
     * @param approxSearchType Type of the local approximation parameter used
     * @param approxSearchParam Value of the local approximation parameter
     * @param mIndex M-Index configuration
     */
    public ApproxSearchCosts(LocalSearchType approxSearchType, int approxSearchParam, MetricIndex mIndex) {
        if (approxSearchType == LocalSearchType.PERCENTAGE) {
            approxSearchType = LocalSearchType.ABS_OBJ_COUNT;
            approxSearchParam = (int) (mIndex.getObjectCount() * ((float) approxSearchParam / 100f));
        }
        this.approxSearchType = approxSearchType;
        this.approxSearchParam = approxSearchParam;
        for (LocalSearchType type : LocalSearchType.values()) {
            costs.put(type, new AtomicInteger(0));
        }
    }
    
    /** 
     * Constructor with given M-Index structure - its default configuration is used.
     * @param mIndex M-Index structure
     */
    public ApproxSearchCosts(MetricIndex mIndex) {
        this(mIndex.getApproxDefaultType(), mIndex.getApproxDefaultParam(), mIndex);
    }

    @Override
    protected ApproxSearchCosts clone() throws CloneNotSupportedException {
        ApproxSearchCosts clone = (ApproxSearchCosts) super.clone();
        clone.approxSearchParam = approxSearchParam;
        clone.approxSearchType = approxSearchType;
        clone.costs = new EnumMap<>(LocalSearchType.class);
        for (LocalSearchType type : LocalSearchType.values()) {
            clone.costs.put(type, new AtomicInteger(costs.get(type).get()));
        }
        return clone;
    }
    
    
    /**
     * Static creator for approximate search cost object given an approximate operation and M-Index to run on.
     * @param operation approximate operation (kNN or Range)
     * @param mIndex M-Index to execute the operation on
     * @return new approx search cost counter
     */
    public static ApproxSearchCosts getMIndexApproxSearchCosts(Approximate operation, MetricIndex mIndex) {
        if (Approximate.LocalSearchType.USE_STRUCTURE_DEFAULT.equals(operation.getLocalSearchType()) ||
                mIndex.isForceDefaultApprox()) {
            return new ApproxSearchCosts(mIndex);
        } else {
            return new ApproxSearchCosts(operation.getLocalSearchType(), operation.getLocalSearchParam(), mIndex);
        }
    }
    
    /**
     * Returns actual value of given approximation parameter type.
     * @param type type of the approximation parameter to return 
     * @return actual value of given approximation parameter type or "0", if this type is not observed 
     */
    public int getSeachCosts(LocalSearchType type) {
        return costs.containsKey(type) ? costs.get(type).get() : 0;
    }
    
    /**
     * Returns difference between the approx. search cost parameter and actual costs already spent.
     * @return difference between the approx. search cost parameter and actual costs already spent
     */
    public int getRemainingCosts() {
        if (approxSearchType == null) {
            return -1;
        }
        if (!costs.containsKey(approxSearchType)) {
            return Integer.MAX_VALUE;
        }
        return approxSearchParam - costs.get(approxSearchType).get();
    }

    /**
     * Checks the approximate condition specified in the constructor of this object by {@link #approxSearchType} and {@link #approxSearchParam}.
     *  If not specified, return false.
     * @return 
     */
    public boolean checkCostCondition() {
        return getRemainingCosts() > 0;
    }
    
    /**
     * Returns the maximal number of objects to be accessed in a cluster while the approx. condition is not broken.
     *  The returned value may be imprecise.
     * @return the maximal number of objects to be accessed in a cluster while the approx. condition is not broken
     */
    public int getRemainingAccessedObjects() {
        int maxAccessedObjects = Integer.MAX_VALUE; 
        switch (approxSearchType) {
            case ABS_OBJ_COUNT: 
                maxAccessedObjects = getRemainingCosts();
                break;
            case BLOCK_READS:
                maxAccessedObjects = objectsForBlockReads(getRemainingCosts());
                break;
            case ABS_DC_COUNT:
                maxAccessedObjects = predictAccessedObjects(getRemainingCosts());
                break;
        }
        return maxAccessedObjects;
    }    
    
    /**
     * Reset the internal counters of this search cost object.
     */
    public void reset() {
        for (AtomicInteger counter : costs.values()) {
            counter.set(0);
        }
    }
    
    /** 
     * Given a type of costs and value, this method updates current statistics accordingly - ONLY this type.
     * 
     * @param type type of the search costs according to {@link ApproxKNNQueryOperation#LocalSearchType}
     * @param difference value by which this costs should be updated
     */
    public void updateSingleCosts(LocalSearchType type, int difference) {
        costs.get(type).addAndGet(difference);
    }

    /**
     * This method is to be called after a local bucket is processed, not all its data was accessed and some 
     *  distance computations might have been saved.
     * @param accessedObjects 
     * @param distanceComputations 
     */
    public void updateAfterSearch(int accessedObjects, int distanceComputations) {
        costs.get(LocalSearchType.ABS_OBJ_COUNT).addAndGet(accessedObjects);
        costs.get(LocalSearchType.BLOCK_READS).addAndGet(blockReadsForObjects(accessedObjects));
        costs.get(LocalSearchType.ABS_DC_COUNT).addAndGet(distanceComputations);
        costs.get(LocalSearchType.DATA_PARTITIONS).addAndGet(1);
    }

    /**
     * Given another search costs object and a bucket to run query on, this method update current statistics as if:
     *  - all data from the bucket will be read sequentially (maximally the "costs" object provided)
     *  - distance will be calculated to all the data (maximally the "costs" object provided)
     * @param costsToUpdateFrom costs limitation (applied only if {@code costsToUpdateFrom.getApproxSearchType() == LocalSearchType.ABS_OBJ_COUNT}
     * @param bucket bucket to predict the search costs on
     */
    public void updateFromPredictMaxCosts(ApproxSearchCosts costsToUpdateFrom, int maxAccessedObjects) {
        if (costsToUpdateFrom.getApproxSearchType() == LocalSearchType.ABS_OBJ_COUNT) {
            maxAccessedObjects = Math.min(maxAccessedObjects, costsToUpdateFrom.getRemainingCosts());
        }
        int maxDC = predictDC(maxAccessedObjects);
        if (costsToUpdateFrom.getApproxSearchType() == LocalSearchType.ABS_DC_COUNT) {
            maxDC = Math.min(maxDC, costsToUpdateFrom.getRemainingCosts());
            maxAccessedObjects = predictAccessedObjects(maxDC);
        }
        updateAfterSearch(maxAccessedObjects, maxDC);
    }

    /**
     * Given another search costs object and a bucket to run query on, this method update current statistics as if:
     *  - all data from the bucket will be read sequentially (maximally the "costs" object provided)
     *  - distance will be calculated to all the data (maximally the "costs" object provided)
     * @param leaf M-Index leaf to predict the costs for
     */
    public void updatePredictMaxCosts(VoronoiLeafCell leaf) {
        ApproxSearchCosts remainingCosts = this;
        if (leaf instanceof MIndexLeafCell) {
            MIndexLeafCell dataLeaf = (MIndexLeafCell) leaf;
            if ((dataLeaf.getStorage() instanceof AlgorithmStorageBucket) && (((AlgorithmStorageBucket) dataLeaf.getStorage()).getAlgorithm() instanceof MIndexAlgorithm)) {
                remainingCosts = new ApproxSearchCosts(((MIndexAlgorithm) ((AlgorithmStorageBucket) dataLeaf.getStorage()).getAlgorithm()).getmIndex());
            }
        }
        updateFromPredictMaxCosts(remainingCosts, leaf.getObjectCount());
    }
    
    /**
     * Given a number of objects to be accessed, this method predicts the number of dist. comp. according 
     *  to the history.
     * @param accessedObjects number of objects to be accessed
     * @return predicted number of dist. comp. to be performed if the objects are accessed
     */
    private int predictDC(int accessedObjects) {
        if (costs.get(LocalSearchType.ABS_OBJ_COUNT).get() <= 0) {
            return accessedObjects;
        }
        return Math.min(
                (int) (((float) costs.get(LocalSearchType.ABS_DC_COUNT).get() / (float) costs.get(LocalSearchType.ABS_OBJ_COUNT).get()) * accessedObjects),
                accessedObjects);
    }

    /**
     * Given a (maximal) number of dist. computations to be performed, this method predicts the number
     *  of objects that can be accessed  (according to the history).
     * @param distanceComputations number of distances to be computed
     * @return predicted number of objects that can be accessed
     */
    private int predictAccessedObjects(int distanceComputations) {
        if (costs.get(LocalSearchType.ABS_OBJ_COUNT).get() <= 0) {
            return distanceComputations;
        }
        return Math.max(
                (int) (((float) costs.get(LocalSearchType.ABS_OBJ_COUNT).get() / (float) costs.get(LocalSearchType.ABS_DC_COUNT).get()) * distanceComputations),
                distanceComputations);
    }
    
    /**
     * Set current thread statistics according to values stored in this object
     */
    public void setThreadStatistics() {
        //OperationStatistics.getOpStatisticCounter("DistanceComputations").set(costs.get(LocalSearchType.ABS_DC_COUNT).get());
        OperationStatistics.getOpStatisticCounter("BlockReads").set(costs.get(LocalSearchType.BLOCK_READS).get());
        OperationStatistics.getOpStatisticCounter("AccessedObjects").set(costs.get(LocalSearchType.ABS_OBJ_COUNT).get());
        OperationStatistics.getOpStatisticCounter("AccessedCells").set(costs.get(LocalSearchType.DATA_PARTITIONS).get());
//        String [] stats = new String [] {"DistanceComputations", "BlockReads", "AccessedObjects", "AccessedCells"};
//        System.out.println("On single M-Index: ");
//        for (String string : stats) {
//            System.out.print("\t" +string+": " + OperationStatistics.getOpStatisticCounter(string).get() +", ");
//        }
//        System.out.println();
    }

    @Override
    public String toString() {
        return Arrays.toString(costs.entrySet().toArray());
    }
 
    
    
}
