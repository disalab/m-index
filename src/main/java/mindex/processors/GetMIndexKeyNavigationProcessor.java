/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.processors;

import java.util.List;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import messif.operations.AbstractOperation;
import mindex.MetricIndex;
import mindex.algorithms.GetMIndexKeysOperation;
import mindex.navigation.VoronoiLeafCell;

/**
 * Navigation processor for the special operation that reads the M-Index keys into 
 *  the {@link ObjectFeature} objects.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
@Deprecated
public class GetMIndexKeyNavigationProcessor extends InsertOperationNavigationProcessor {

    /**
     * Initializes the processor for a single-object insert operation. The object is initialized
     *  by M-Index (set M-Index pivot permutation) and added to the cell map.
     * @param operation single-object insert operation
     * @param mIndex M-Index object
     * @throws AlgorithmMethodException if the initialization goes wrong
     */
    public GetMIndexKeyNavigationProcessor(GetMIndexKeysOperation operation, MetricIndex mIndex) throws AlgorithmMethodException {
        super(operation, mIndex);
    }

    /**
     * This method adds the created M-Index key from the filter (MIndexKey) to the {@link ObjectFeature}
     * @param objects to be processed
     */
    @Override
    protected void processObjectsOnLeaf(VoronoiLeafCell leaf, List<LocalAbstractObject> objects) throws AlgorithmMethodException {
        for (LocalAbstractObject obj : objects) {
//            if (obj instanceof ObjectFeature) {
//                MIndexPP mIndexPP = MetricIndexes.readMIndexPP(obj);
//                ((ObjectFeature) obj).addKey(mIndexPP.getClusterNumber(leaf.getLevel()));
//                obj.unchainFilter((PrecomputedDistancesFilter) mIndexPP);
                throw new AlgorithmMethodException("TODO: rewrite GetMIndexKey using the MetricIndex method.");
//            }
        }
    }

    @Override
    public void close() {
        super.close();
    }    
 
    /**
     * Factory method for creating an instance of InsertOperationNavigationProcessor for given Bulk/InsertOperation
     * If allowed - initializing the objects (computing distances to pivots) is done in parallel. Acquires write lock on the leaf nodes.
     * @param operation - either instance of InsertOperation or BulkInsertOperation
     * @param mIndex
     * @return initialized instance of InsertOperationNavigationProcessor
     * @throws AlgorithmMethodException 
     */
    public static GetMIndexKeyNavigationProcessor getInstance(AbstractOperation operation, MetricIndex mIndex) throws AlgorithmMethodException {
        if (operation instanceof GetMIndexKeysOperation) {
            return new GetMIndexKeyNavigationProcessor((GetMIndexKeysOperation) operation, mIndex);
        } else {
            throw new UnsupportedOperationException("InsertOperationNavigationProcessor can be created only for InsertOperation or BulkInsertOperation");
        }
    }
}
