/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.processors;

import java.util.Iterator;
import java.util.Map.Entry;
import messif.algorithms.AlgorithmMethodException;
import messif.buckets.LocalBucket;
import messif.buckets.impl.MemoryStorageMIndexKeyBucket;
import messif.buckets.index.Search;
import messif.buckets.index.SearchAbstractObjectIterator;
import messif.objects.LocalAbstractObject;
import messif.operations.query.ApproxKNNQueryOperation;
import messif.operations.query.RangeQueryOperation;
import messif.statistics.OperationStatistics;
import mindex.MIndexKey;
import mindex.MIndexKeyInterval;
import mindex.MIndexKeyPPFull;
import mindex.MIndexKeySimple;
import mindex.MIndexPP;
import mindex.MetricIndex;
import mindex.navigation.VoronoiCell;
import mindex.navigation.VoronoiLeafCell;
import mindex.navigation.MIndexLeafCell;
import mindex.navigation.MIndexPreciseInternalCell;
import mindex.navigation.MIndexPreciseLeafCell;

/**
 * Navigation processor for the precise range queries. It first determines those leaf cells that cannot be 
 *  pruned using repetitive application of the double-pivot constraint (or range pivot constraint). 
 *  Then, each of these leaves can be processed - using intervals within each leaf (object-pivot constraint).
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class RangeNavigationProcessor extends CellQueryOpNavigationProcessor<RangeQueryOperation> {

    /** The M-Index object */
    protected final MetricIndex mIndex;

    /** Distances between the query object and pivots */
    protected final float[] queryPivotDistances;
    
    /** Pivot-permutation indexes corresponding to given query */
    protected final short[] queryIndexes;
    
    /** Query radius (used often, therefore stored in this field) */
    protected final float queryRadius;
    
    /** Thread-safe cache, used when multi-buckets used */
    protected MultiBucketCache cache = null;
    
    /**
     * Creates new approximate navigation processor for given operation and M-Index. The priority queue is initialized.
     * @param operation approximate kNN operation (should be the same for approximate range)
     * @param mIndex M-Index object with configuration and dynamic cell tree
     * @throws AlgorithmMethodException if the M-Index cannot determine the PP for query object
     */
    public RangeNavigationProcessor(RangeQueryOperation operation, MetricIndex mIndex) throws AlgorithmMethodException {
        super(operation);
        if (! mIndex.isPreciseSearch()) {
            throw new AlgorithmMethodException("The range query can be processed only on precise M-Index");
        }
        this.mIndex = mIndex;
        
        queryPivotDistances = mIndex.initPPGetDistances(operation.getQueryObject());
        queryIndexes = mIndex.readPPP(operation.getQueryObject());
        queryRadius = operation.getRadius();
        
        if (mIndex.isMultiBuckets()) {
            cache = new MultiBucketCache();
        }
        
        fillRelevantNodes((MIndexPreciseInternalCell) mIndex.getVoronoiCellTree());
        closeQueue();
    }

    /**
     * Recursive method to traverse the M-Index dynamic cell tree in a breadth-first-manner
     *  and identify all leaf cell that cannot be filtered out using repetitive application
     *  of the double-pivot constraint.
     * @param treeRoot current internal node for tree traversal
     */
    private void fillRelevantNodes(MIndexPreciseInternalCell treeRoot) {
        int level = treeRoot.getLevel() + 1;
        //MIndexPP pivotCombination = treeRoot.                getPivotPermutation();
        short[] rootPPP = treeRoot.getPppForReading();
        // find the smallest pivot index, that was not used in higher cluster-tree levels
        int queryNextLevelToConsider = -1;
        boolean isActualFilteringIndexInCombination;
        do {
            queryNextLevelToConsider++;
            isActualFilteringIndexInCombination = false;
            //for (short i = 1; i <= level - 1; i++ ) {
            for (short i = 0; i < level - 1; i++ ) {
                //if (pivotCombination.getPivotIndex(i) == queryIndexes[queryNextLevelToConsider]) {
                if (rootPPP[i] == queryIndexes[queryNextLevelToConsider]) {
                    isActualFilteringIndexInCombination = true;
                    break;
                }
            }
        } while (isActualFilteringIndexInCombination);
        // iterate over all child nodes, try to filter them out and process recursively
        int i = 0;
        for (Iterator<Entry<Short, VoronoiCell<LocalAbstractObject>>> iterator = treeRoot.getChildNodes(); iterator.hasNext(); i++) {
            VoronoiCell child = iterator.next().getValue();
            if (child == null) {
                continue;
            }
        
            if (queryPivotDistances[i] - queryPivotDistances[queryIndexes[queryNextLevelToConsider]] > 2f * queryRadius) {
                continue;
            }
            if (child instanceof MIndexPreciseInternalCell) {
                fillRelevantNodes((MIndexPreciseInternalCell) child);
            } else {
                MIndexPreciseInternalCell newValidCell = (MIndexPreciseInternalCell) addLeafNode((VoronoiLeafCell) child);
                // if the leaf cell is not valid any more (it was split)
                if (newValidCell != null) {
                    fillRelevantNodes(newValidCell);
                }
            }
        }
    }
    
    /**
     * Creates interval of keys relevant for given Voronoi cell and radius
     * @param leaf M-Index Voronoi leaf cell
     * @param radius actual query radius
     * @param queryPivotDistances array of distances between the query and the pivots
     * @param mIndex M-Index settings object
     * 
     * @return interval of keys relevant for given Voronoi cell and radius
     */
    protected static MIndexKeyInterval createRelevantInterval(MIndexPreciseLeafCell leaf, float radius, float [] queryPivotDistances, MetricIndex mIndex) {
        short level = leaf.getLevel(); 
        MIndexPP pivotCombination = leaf.getPivotPermutation();
        float distance = queryPivotDistances[pivotCombination.getPivotIndex((short) 1)];
        
        MIndexKey intervalMin = new MIndexKeyPPFull(pivotCombination.getPPPForReading(), mIndex.getMaxLevel(), mIndex.getNumberOfPivots(), Math.max(distance - radius, 0f) / mIndex.getMaxDistance(), level);
        MIndexKey intervalMax = (distance + radius >= mIndex.getMaxDistance()) ? new MIndexKeySimple(intervalMin.getClusterNumber() + 1, 0f) :
                new MIndexKeySimple(intervalMin.getClusterNumber(), (distance + radius) / mIndex.getMaxDistance());
        
        // the interval uses the object-pivot distance constraint
        return new MIndexKeyInterval(intervalMin, intervalMax);        
    }

    /**
     * Creates interval of keys relevant for given Voronoi cell and radius
     * @param leaf M-Index Voronoi leaf cell
     * @param radius actual query radius
     * @return interval of keys relevant for given Voronoi cell and radius
     */
    protected MIndexKeyInterval createRelevantInterval(MIndexPreciseLeafCell leaf, float radius) {
        return createRelevantInterval(leaf, radius, queryPivotDistances, mIndex);
    }

    /**
     * Given a leaf cell and M-Index key interval, this method creates an iterator over
     *  objects from this leaf whose keys are within this interval. If multi-buckets
     *  are used, the passed "cache" is used.
     * @param leaf M-Index leaf cell with data (with (multi-)bucket)
     * @param interval interval of M-Index keys generated by given query (range or kNN)
     * @param cache multi-bucket cache (can be null if multi-buckets are not used)
     * @param mIndex corresponding M-Index configuration
     * @return an iterator over objects from this leaf whose keys are within this interval
     * @throws AlgorithmMethodException if the processing goes wrong
     * @throws IllegalStateException if the underlying collection is modified in the meanwhile (should not happen)
     */
    protected static SearchAbstractObjectIterator getIntervalIterator(MIndexLeafCell leaf, MIndexKeyInterval interval, MultiBucketCache cache, MetricIndex mIndex) throws AlgorithmMethodException, IllegalStateException {
        Search objectsWithinInterval = (mIndex.isMultiBuckets()) ?
               cache.processBucketWithInterval(leaf.getStorage(), leaf, MemoryStorageMIndexKeyBucket.createMIndexKeyComparator(leaf.getLevel()), interval) :
               ((LocalBucket) leaf.getStorage()).getIndex().search(MemoryStorageMIndexKeyBucket.createMIndexKeyComparator(leaf.getLevel()), interval.getFrom(), interval.getTo());
        SearchAbstractObjectIterator iterator = new SearchAbstractObjectIterator<>(objectsWithinInterval, Integer.MAX_VALUE);

        if (! mIndex.isMultiBuckets()) {
            OperationStatistics.getOpStatisticCounter("AccessedBuckets").add();
        }
        return iterator;
    }    
    
    /**
     * Process the approximate query on the data from given M-Index Voronoi leaf cell 
     *  (call standard {@link ApproxKNNQueryOperation#evaluate(messif.objects.util.AbstractObjectIterator) } on relevant data).
     * @param leaf M-Index Voronoi leaf cell
     * @return number of objects processed (accessed) by the operation
     * @throws AlgorithmMethodException if the evaluate method fails
     */
    @Override
    protected int processOnLeaf(RangeQueryOperation operation, VoronoiLeafCell leaf) throws AlgorithmMethodException {
        // TODO: for remote buckets, the intervals should be used by the 
        // inteligent buckets in the other side; 
        // TODO: for remote multi-buckets, the intervals should be grouped and then send
        if (! (leaf instanceof MIndexPreciseLeafCell)) {
            throw new AlgorithmMethodException("the range operation can be processed only on MIndexLeafCellData");
        }
        MIndexPreciseLeafCell dataLeaf = (MIndexPreciseLeafCell) leaf;
        if (dataLeaf.getStorage() == null) {
            return 0;
        }
        if (!(dataLeaf.getStorage() instanceof LocalBucket)) {
            leaf.processOperation(operation);
            OperationStatistics.getOpStatisticCounter("AccessedBuckets").add();
            return leaf.getObjectCount();
        }
        
        MIndexKeyInterval interval = createRelevantInterval(dataLeaf, queryRadius);
        // try to filter using range-pivot distance constraint
        if (dataLeaf.getCoveredInterval() != null && ! dataLeaf.getCoveredInterval().intersect(interval)) {
            return 0;
        }
        
        SearchAbstractObjectIterator iterator = getIntervalIterator(dataLeaf, interval, cache, mIndex);
        
        // evaluate the operation on relevant data
        operation.evaluate(iterator);

        return iterator.getCount();
    }

}