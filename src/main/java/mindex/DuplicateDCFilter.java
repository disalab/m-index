/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import messif.objects.LocalAbstractObject;
import messif.objects.PrecomputedDistancesFilter;

/**
 * This filter checks that each data object is processed only once (this makes
 * sense only if multi-overlay M-Index used).
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class DuplicateDCFilter extends PrecomputedDistancesFilter {

    private static final long serialVersionUID = 1271562657309916178L;
    
    protected final Object checkedObject;
    
    //protected final TObjectIntMap<Object> processedObjects;
    protected final Set<Object> processedObjects;

//    public void setProcessedObjects(THashSet processedObjects) {
//        this.processedObjects = processedObjects;
//    }        
//    private transient int noEntryValue;
    
    public DuplicateDCFilter(Object cheObject) {
        this(cheObject, null);
    }

    public DuplicateDCFilter(Object cheObject, Set<Object> processedObjects) {
        this.checkedObject = cheObject;
        this.processedObjects = processedObjects;
//        if (processedObjects != null) {
//            noEntryValue = processedObjects.getNoEntryValue();
//        }
    }
    
    @Override
    public boolean excludeUsingPrecompDist(PrecomputedDistancesFilter targetFilter, float radius) {
//        return noEntryValue != processedObjects.putIfAbsent(((DuplicateDCFilter)targetFilter).checkedObject, 1);
        //synchronized (processedObjects) {
//            return null != processedObjects.putIfAbsent(((DuplicateDCFilter)targetFilter).checkedObject, ((DuplicateDCFilter)targetFilter).checkedObject);
            return ! processedObjects.add(((DuplicateDCFilter)targetFilter).checkedObject);
        //}
    }

    // **************      The rest is not supported       ********************************** //
    
    @Override
    public boolean includeUsingPrecompDist(PrecomputedDistancesFilter targetFilter, float radius) {
        return false;
    }

    @Override
    public float getPrecomputedDistance(LocalAbstractObject obj, float[] metaDistances) {
        return LocalAbstractObject.UNKNOWN_DISTANCE;
    }
    
    @Override
    protected boolean addPrecomputedDistance(LocalAbstractObject obj, float distance, float[] metaDistances) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void writeData(OutputStream stream) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected boolean isDataWritable() {
        return false;
    }
    
}
