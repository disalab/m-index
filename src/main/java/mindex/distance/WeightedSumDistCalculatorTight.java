/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.distance;

import messif.algorithms.AlgorithmMethodException;
import messif.operations.RankingSingleQueryOperation;
import mindex.MetricIndex;
import mindex.navigation.NextLevelPivotChecker;

/**
 * This query-PPP distance calculator uses the query-pivot distances as in {@link WeightedSumDistCalculator}
 *  and tries to estimate the minimum distance to objects in the tested subtree as possible. It 
 *  actually uses the {@link NextLevelPivotChecker} to look "forward" to pivot indexes actually stored on the next level.
 * 
 *  THIS IS THE DEFAULT AND RECOMMENDED DISTANCE CALCULATOR FOR BOTH M-INDEX AND PPP-CODES
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class WeightedSumDistCalculatorTight extends WeightedSumDistCalculator {

    public WeightedSumDistCalculatorTight(RankingSingleQueryOperation operation, MetricIndex mIndex) throws AlgorithmMethodException {
        super(operation, mIndex);
    }
    
    protected WeightedSumDistCalculatorTight(short maxLevel, float [] queryPivotDistances, short [] queryPPP) {
        super(maxLevel, queryPivotDistances, queryPPP);
    }

    // **************   Distance calculations    ************************** //

    @Override
    public final float getQueryDistance(final short[] ppp) {
        return getQueryDistance(ppp, null);
    }

    @Override
    public float getQueryDistance(short[] ppp, NextLevelPivotChecker cell) {
        int length = getMaxLevel(ppp.length);
        float sum = 0f;
        for (int i = 0; i < length; i++) {
            sum += (queryPivotDistances[ppp[i]] * COEFS[i]);
        }
        
        // correction to estimate the minimum possible distance of data with this ppp prefix
        int smallestNonusedIndex = 0;
        int usedFirstIndex = -1;
        for (int i = length; i < maxLevel; i++) {
            if (smallestNonusedIndex == usedFirstIndex) {
                smallestNonusedIndex++;
            }

            int j = 0;
            while (j < length) {
                if (queryPP[smallestNonusedIndex] == ppp[j++]) {
                    if (++smallestNonusedIndex == usedFirstIndex) {
                        smallestNonusedIndex++;
                    }
                    j = 0;
                }
            }

            // check the really existing nodes on the next level
            if (i == length && cell != null) {
                usedFirstIndex = smallestNonusedIndex;
                while (! cell.containsGivenPivot(queryPP[usedFirstIndex])) {
                    usedFirstIndex++;
                }
                sum += queryPivotDistances[queryPP[usedFirstIndex]] * COEFS[i];
            } else {
                sum += queryPivotDistances[queryPP[smallestNonusedIndex++]] * COEFS[i];
            }
        }
        return sum;
    }
    
}
