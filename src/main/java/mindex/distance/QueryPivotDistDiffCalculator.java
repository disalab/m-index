/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.distance;

import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import mindex.MIndexObjectInitilizer;
import mindex.MetricIndex;
import mindex.navigation.NextLevelPivotChecker;

/**
 * This class calculates distance between a query object represented by query-pivot distances
 *  and a data object PPP using the "sum of query-pivot distance differences" normalized by
 *  the level (length of PPP).
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class QueryPivotDistDiffCalculator implements QueryPPPDistanceCalculator {

    /** Distance to all the pivots (can be set) */
    protected final float [] queryPivotDistances;
    
    /** PPP of the query */
    protected final short [] queryPP;

    protected volatile int distanceCalculator = 0;
    
    /** This field specifies the maximal level of PPP to be used (M-Index max level). */
    protected int maxLevel;
    
    @Override
    public float[] getQueryPivotDistances() {
        return queryPivotDistances;
    }

    @Override
    public short[] getQueryPPP() {
        return queryPP;
    }

    @Override
    public int getDistCalculationStat() {
        return distanceCalculator;
    }

    @Override
    public final int getMaxLevel() {
        return maxLevel;
    }

    @Override
    public final int getMaxLevel(int currentMax) {
        return (currentMax < maxLevel) ? currentMax : maxLevel;
    }

    @Override
    public final void setMaxLevel(int maxLevel) {
        this.maxLevel = maxLevel;
    }
        
    
    /**
     * Creates the calculator from given query object and the M-Index configuration
     * @param queryObject query object
     * @param mIndex M-Index configuration
     */
    public QueryPivotDistDiffCalculator(LocalAbstractObject queryObject, MetricIndex mIndex) throws AlgorithmMethodException {
        maxLevel = mIndex.getMaxLevel();
        queryPivotDistances = mIndex.initPPGetDistances(queryObject);
        queryPP = MIndexObjectInitilizer.sortDistances(queryPivotDistances, (short) queryPivotDistances.length);
        //queryPP = mIndex.readPPP(queryObject);
    }

    /**
     * For testing purposes 
     */
    protected QueryPivotDistDiffCalculator(short maxLevel, float [] queryPivotDistances, short [] queryPPP) {
        this.maxLevel = maxLevel;
        this.queryPivotDistances = queryPivotDistances;
        this.queryPP = queryPPP;
    }
    
    // ************** Query-PPP distance calculation ***************//

    @Override
    public float getQueryDistance(short[] ppp) {        
        float sum = 0f;
        for (int i = 0; i < getMaxLevel(ppp.length); i++) {
            sum += Math.max(queryPivotDistances[ppp[i]] - queryPivotDistances[queryPP[i]], 0f);
        }
        return sum;
    }

    @Override
    public float getQueryDistance(short[] ppp, NextLevelPivotChecker cell) {
        return getQueryDistance(ppp);
    }    
}
