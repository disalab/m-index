/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.distance;

import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import messif.operations.RankingSingleQueryOperation;
import mindex.MetricIndex;

/**
 * This query-PPP distance calculator uses the query-pivot distances as in {@link QueryPivotDistDiffCalculator}
 *  and adds a "correction" to the calculated distance. The correction results in a value that is still 
 *  a lower bound on the distances between the query and objects on the last level (l) but this
 *  lower bound is tighter (better). 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class WeightedSumDistCalculatorCorrected extends WeightedSumDistCalculator implements PartialQueryPPPDistanceCalculator {
    
    private float [] corrections;

    public WeightedSumDistCalculatorCorrected(RankingSingleQueryOperation operation, MetricIndex mIndex) throws AlgorithmMethodException {
        super(operation, mIndex);
        initCorrections();
    }
    
    public WeightedSumDistCalculatorCorrected(LocalAbstractObject queryObject, MetricIndex mIndex, float coeficient) throws AlgorithmMethodException {
        super(queryObject, mIndex, coeficient);
        initCorrections();
    }
    
    protected WeightedSumDistCalculatorCorrected(short maxLevel, float [] queryPivotDistances, short [] queryPPP) {
        super(maxLevel, queryPivotDistances, queryPPP);
        initCorrections();
    }
    
    private void initCorrections() {
        corrections = new float[maxLevel + 1];
        // calculate the corrections for individual levels (lengths of PPP)
        for (int lstripe = maxLevel - 1; lstripe >= 0; lstripe--) {
            for (int j = lstripe; j <= maxLevel - 1; j++) {
                corrections[lstripe] += COEFS[j] * queryPivotDistances[queryPP[j - lstripe]];
            }
        }
    }

    // **************   Distance calculations    ************************** //
    
    @Override
    public final float getQueryDistance(final short[] ppp) {
        return getDistance(ppp, 1, getMaxLevel(ppp.length));
    }

    @Override
    public float getPartialQueryDistance(final short[] ppp, final int fromLevel) {
        if (ppp.length == 0) {
            return 0;
        }
        return getDistance(ppp, fromLevel, getMaxLevel(fromLevel - 1 + ppp.length)) - corrections[fromLevel - 1];
    }

    private float getDistance(final short[] ppp, final int fromLevel, final int toLevel) {
        float sum = 0f;
        for (int i = 0; i <= toLevel - fromLevel; i++) {
            sum += (queryPivotDistances[ppp[i]] * COEFS[i + fromLevel - 1]);
        }
        return sum + corrections[toLevel];
    }

    @Override
    public float getPartialQueryDistance(short ppIndex, int fromLevel) {
        return (queryPivotDistances[ppIndex] * COEFS[fromLevel - 1]) - corrections[fromLevel - 1] + corrections[fromLevel];
    }
        
}
