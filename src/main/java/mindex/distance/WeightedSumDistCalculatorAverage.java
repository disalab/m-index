/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.distance;

import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import messif.operations.RankingSingleQueryOperation;
import mindex.MetricIndex;

/**
 * This query-PPP distance calculator uses the query-pivot distances as in {@link QueryPivotDistDiffCalculator}
 *  and adds a "correction" to the calculated distance. The correction results in a value that is still 
 *  a lower bound on the distances between the query and objects on the last level (l) but this
 *  lower bound is tighter (better). 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class WeightedSumDistCalculatorAverage extends WeightedSumDistCalculator { //implements PartialQueryPPPDistanceCalculator {
    
    public WeightedSumDistCalculatorAverage(RankingSingleQueryOperation operation, MetricIndex mIndex) throws AlgorithmMethodException {
        super(operation, mIndex);
    }
    
    public WeightedSumDistCalculatorAverage(LocalAbstractObject queryObject, MetricIndex mIndex, float coeficient) throws AlgorithmMethodException {
        super(queryObject, mIndex, coeficient);
    }
    
    protected WeightedSumDistCalculatorAverage(short maxLevel, float [] queryPivotDistances, short [] queryPPP) {
        super(maxLevel, queryPivotDistances, queryPPP);
    }
    
    // **************   Distance calculations    ************************** //
    
    @Override
    public float getQueryDistance(short[] ppp) {        
        return getCellDistanceEstimation(ppp);
    }
        
}
