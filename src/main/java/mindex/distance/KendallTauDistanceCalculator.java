/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.distance;

import java.util.Arrays;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import mindex.MIndexObjectInitilizer;
import mindex.MetricIndex;
import mindex.navigation.NextLevelPivotChecker;

/**
 * This class calculates distance between a query object represented by query-pivot distances
 *  and a data object PPP using the "sum of query-pivot distance differences" normalized by
 *  the level (length of PPP).
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class KendallTauDistanceCalculator implements QueryPPPDistanceCalculator {

    /**  */
    protected final short [] fullQueryPP;

    /**  */
    protected final short [] invertedQueryPP;
    
    /**  */
    protected final int normalizationFactor;
    

    /** Distance to all the pivots (can be set) */
    protected final float [] queryPivotDistances;
    
    /** PPP of the query */
    protected final short [] queryPPP;

    protected volatile int distanceCounter = 0;
    
    /** For testing reasons, this field specifies the maximal level of PPP to be used*/
    protected int maxLevel;
    
    @Override
    public float[] getQueryPivotDistances() {
        return queryPivotDistances;
    }

    @Override
    public short[] getQueryPPP() {
        return queryPPP;
    }

    @Override
    public int getDistCalculationStat() {
        return distanceCounter;
    }
    
    @Override
    public final int getMaxLevel() {
        return maxLevel;
    }

    @Override
    public final int getMaxLevel(int currentMax) {
        return (currentMax < maxLevel) ? currentMax : maxLevel;
    }
    
    @Override
    public final void setMaxLevel(int maxLevel) {
        this.maxLevel = maxLevel;
    }
    
    /**
     * Creates the calculator from given query object and the M-Index configuration
     * @param queryObject query object
     * @param mIndex M-Index configuration
     */
    public KendallTauDistanceCalculator(LocalAbstractObject queryObject, MetricIndex mIndex) throws AlgorithmMethodException {
        maxLevel = mIndex.getMaxLevel();
        queryPivotDistances = mIndex.initPPGetDistances(queryObject);
        queryPPP = mIndex.readPPP(queryObject);

        fullQueryPP = MIndexObjectInitilizer.sortDistances(queryPivotDistances, mIndex.getNumberOfPivots());

        invertedQueryPP = initInvertedPP();
        normalizationFactor = (fullQueryPP.length * (fullQueryPP.length - 1)) / 2;
    }

    private short [] initInvertedPP() {
        short [] retval = new short[fullQueryPP.length];
        for (short i = 0; i < fullQueryPP.length; i++) {
            retval[fullQueryPP[i]] = i;
        }
        return retval;
    }

    public KendallTauDistanceCalculator(short[] fullQueryPP, int maxLevel) {
        this.fullQueryPP = fullQueryPP;
        this.maxLevel = maxLevel;

        this.queryPivotDistances = null;
        
        this.queryPPP = Arrays.copyOf(fullQueryPP, maxLevel);
        this.invertedQueryPP = initInvertedPP();
        this.normalizationFactor = (fullQueryPP.length * (fullQueryPP.length - 1)) / 2;
    }
    
    // ************** Query-PPP distance calculation ***************//

    @Override
    public float getQueryDistance(short[] ppp) {        
        int sum = 0;
        short [] seenPositions = new short [getMaxLevel(ppp.length)];
        for (int i = 0; i < seenPositions.length; i++) {
            short pivotPositionInQueryPP = invertedQueryPP[ppp[i]];
            
            int numberOfSmallerPositions = 0;
            while (numberOfSmallerPositions < i && seenPositions[numberOfSmallerPositions] < pivotPositionInQueryPP) {
                numberOfSmallerPositions ++;
            }
            System.arraycopy(seenPositions, numberOfSmallerPositions, seenPositions, numberOfSmallerPositions + 1, i - numberOfSmallerPositions);
            seenPositions[numberOfSmallerPositions] = pivotPositionInQueryPP;
            
            sum += pivotPositionInQueryPP - numberOfSmallerPositions;
        }
        
        return (float) sum / (float) normalizationFactor;
    }

    @Override
    public float getQueryDistance(short[] ppp, NextLevelPivotChecker cell) {
        return getQueryDistance(ppp);
    }    
    
}
