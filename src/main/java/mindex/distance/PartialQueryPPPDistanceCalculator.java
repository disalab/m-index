/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.distance;

/**
 * Extension of the {@link QueryPPPDistanceCalculator} that enables to calculate only
 *  partial value of the distance starting from given level.
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public interface PartialQueryPPPDistanceCalculator extends QueryPPPDistanceCalculator {
    
    /**
     * Calculate partial distance between a query (typically represented by full PP or query-pivot
     *  distances) and given pivot permutation prefix. Skip the [1, fromLevel) levels from the calculation.
     * @param ppp pivot permutation prefix of some length
     * @param fromLevel the level from which this partial distance should be calculated
     * @return partial distance between query and given PPP
     */
    public float getPartialQueryDistance(final short [] ppp, final int fromLevel);
            
    /**
     * Calculate partial distance between a query (typically represented by full PP or query-pivot
     *  distances) and given pivot permutation prefix. Skip the [1, fromLevel) levels from the calculation.
     * @param ppIndex pivot permutation item at level {@code fromLevel}
     * @param fromLevel the level from which this partial distance should be calculated
     * @return partial distance between query and given PPP
     */
    public float getPartialQueryDistance(final short ppIndex, final int fromLevel);
    
}
