/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import messif.objects.LocalAbstractObject;
import messif.objects.PrecomputedDistancesFilter;
import messif.objects.nio.BinaryInput;
import messif.objects.nio.BinaryOutput;
import messif.objects.nio.BinarySerializable;
import messif.objects.nio.BinarySerializator;

     
/**
 * M-Index object key that contains only the pivot permutation prefix.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class MIndexPPSimple extends PrecomputedDistancesFilter implements MIndexPP, BinarySerializable {

    /** Class serial id for serialization. */
    private static final long serialVersionUID = 721101L;

    /** The pivot permutation encoded as long */
    private long clusterPPEncoded;

    /** Max M-Index level (for en(de)coding) */
    private short maxLevel;
    
    /** Number of pivots  (for en(de)coding) */
    private short nPivots;

    /** Length of the PP that is known (<code>&lt;= maxLevel</code>) */
    private short ppLength;
    
    
    /** Pivot permutation (up to M-Index maximal level) */
    protected transient short [] pivotPermutation = null;

    /** The level for which the cluster number was precomputed, or -1 */
    protected transient short precomputedLevel = -1;
    
    /** If the {@link #precomputedLevel} is used, this field contains precomputed cluster number for given level. */
    protected transient long clusterNumber;
    

    /**
     * Creates new instance of the key having all necessary values.
     * @param pivotPermutation array of pivot indexes 
     */
    public MIndexPPSimple(short [] pivotPermutation, short maxLevel, short nPivots) {
        this.pivotPermutation = pivotPermutation;
        this.maxLevel = maxLevel;
        this.nPivots = nPivots;
        this.ppLength = (short) pivotPermutation.length;
        this.clusterPPEncoded = computeClusterNumber(maxLevel);
    }

    
    /**
     * This method returns the number of cluster corresponding to this PP at given level; 
     *   it also depends on the maxlevel (size of PP).
     * @return M-Index number of corresponding cluster
     */
    @Override
    public long getClusterNumber(short level) {
        if (precomputedLevel != level) {
            clusterNumber = computeClusterNumber(level);
            precomputedLevel = (short) level;
        }
        return clusterNumber;
    }

    /**
     * This method returns the number of cluster corresponding to this PP an level; 
     *  it also depends on the maxlevel (size of PP).
     * @param level is 1-based
     * @return M-Index number of corresponding cluster
     */
    private long computeClusterNumber(short level) {
        if (pivotPermutation == null) {
            decodePivotPermutation();
        }
        long retVal = 0L;
        long constant = 1L;
        for (int i = maxLevel - 1; i >= 0; i--) {
            if (i < level && i < ppLength) {
                retVal += pivotPermutation[i] * constant;
            }
            constant *= nPivots;
        }
        return retVal;
    }

    /**
     * This method returns the number of cluster corresponding to this PP an level; 
     *  it also depends on the maxlevel (size of PP).
     * @param level is 1-based
     * @return M-Index number of corresponding cluster
     */
    private void decodePivotPermutation() {
        pivotPermutation = new short[ppLength];        
        long constant = 1l;
        for (int i = 0; i < maxLevel - 1; i++) {
            constant *= nPivots;
        }
        long tmpClusterNumber = clusterPPEncoded;
        for (int i = 0; i < maxLevel && i < ppLength; i++) {
            pivotPermutation[i] = (short) (tmpClusterNumber / constant);
            tmpClusterNumber = tmpClusterNumber % constant;
            constant /= nPivots;
        }
    }
    
    @Override
    public short[] getPivotPermutation() {
        if (pivotPermutation == null) {
            decodePivotPermutation();
        }
        return Arrays.copyOf(pivotPermutation, pivotPermutation.length);
    }

    @Override
    public short[] getPPPForReading() {
        if (pivotPermutation == null) {
            decodePivotPermutation();
        }
        return pivotPermutation;
    }
    
    @Override
    public short getPivotIndex(short level) {
        if (pivotPermutation == null) {
            decodePivotPermutation();
        }
        if (level < 1 || level > pivotPermutation.length) {
            return -1;
        }
        return pivotPermutation[level-1];
    }

    @Override
    public short getMaxLevel() {
        return maxLevel;
    }

    /** 
     * Returns the level (length) of this PPP
     */
    public short getPPLevel() {
        return ppLength;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MIndexPPSimple other = (MIndexPPSimple) obj;
        if (this.clusterPPEncoded != other.clusterPPEncoded) {
            return false;
        }
        if (this.maxLevel != other.maxLevel) {
            return false;
        }
        if (this.nPivots != other.nPivots) {
            return false;
        }
        if (this.ppLength != other.ppLength) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (int) (this.clusterPPEncoded ^ (this.clusterPPEncoded >>> 32));
        hash = 29 * hash + this.maxLevel;
        hash = 29 * hash + this.nPivots;
        hash = 29 * hash + this.ppLength;
        return hash;
    }


    //************ BinarySerializable interface ************//

    /**
     * Creates a new instance of MIndexFinalKey loaded from binary input stream.
     * 
     * @param input the stream to read the key from
     * @param serializator the serializator used to write objects
     * @throws IOException if there was an I/O error reading from the stream
     */
    protected MIndexPPSimple(BinaryInput input, BinarySerializator serializator) throws IOException {
        super(input, serializator);
        clusterPPEncoded = serializator.readLong(input);
        maxLevel = serializator.readShort(input);
        nPivots = serializator.readShort(input);
        ppLength = serializator.readShort(input);
    }

    /**
     * Binary-serialize this object into the <code>output</code>.
     * @param output the output stream this object is binary-serialized into
     * @param serializator the serializator used to write objects
     * @return the number of bytes actually written
     * @throws IOException if there was an I/O error during serialization
     */
    @Override
    public int binarySerialize(BinaryOutput output, BinarySerializator serializator) throws IOException {
        return super.binarySerialize(output, serializator) + serializator.write(output, clusterPPEncoded)
                + serializator.write(output, maxLevel) + serializator.write(output, nPivots) + serializator.write(output, ppLength);
    }

    /**
     * Returns the exact size of the binary-serialized version of this object in bytes.
     * @param serializator the serializator used to write objects
     * @return size of the binary-serialized version of this object
     */
    @Override
    public int getBinarySize(BinarySerializator serializator) {
        return super.getBinarySize(serializator) + serializator.getBinarySize(clusterPPEncoded)
               + serializator.getBinarySize(maxLevel) + serializator.getBinarySize(nPivots) + serializator.getBinarySize(ppLength);
    }

    @Override
    public String toString() {
        if (pivotPermutation == null) {
            decodePivotPermutation();
        }
        return (new StringBuilder()).append(Arrays.toString(pivotPermutation)).toString();
    }
    
    
    // TODO: REMOVE WHEN IT WON'T BE FILTER ANY MORE
    
    
    @Override
    public float getPrecomputedDistance(LocalAbstractObject obj, float[] metaDistances) {
        return LocalAbstractObject.UNKNOWN_DISTANCE;
    }

    @Override
    public boolean excludeUsingPrecompDist(PrecomputedDistancesFilter targetFilter, float radius) {
        return false;
    }

    @Override
    public boolean includeUsingPrecompDist(PrecomputedDistancesFilter targetFilter, float radius) {
        return false;
    }

    @Override
    protected boolean addPrecomputedDistance(LocalAbstractObject obj, float distance, float[] metaDistances) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void writeData(OutputStream stream) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected boolean isDataWritable() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
