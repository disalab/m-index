/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.navigation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import messif.buckets.LocalBucket;
import messif.buckets.split.SplitPolicy;
import messif.objects.BallRegion;
import messif.objects.LocalAbstractObject;
import mindex.MIndexPP;
import mindex.MetricIndex;
import mindex.MetricIndexes;

/**
 * This class implements Voronoi-like split policy used in M-Index to split
 *   objects according to their pivot index on given level.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class MultiBucket extends SplitPolicy {

    /** The multi bucket */
    final LocalBucket bucket;
    
    /** The list of leaf nodes that share this multi bucket with the value of their last pivot index */
    //final SortedSet<MIndexLeafCell> nodes = new TreeSet<>(occInverseComparator);
    final Map<MIndexLeafCell,Short> nodes = new HashMap<>();
    
    /** Corresponding MetricIndex object */
    final MetricIndex mIndex;

    
    // *************************    Preparations for multi-bucket split    ********************** //
    
    /** 
     * After split, this array stores information about mapping between the pivot index on given level
     *  and the new physical (multi)-buckets 
     */
    protected int [] nodesBucketMapping = null;
    
    /** Policy parameter <i>level</i> */
    short level = 1;
    
    int resultingBuckets = 1;

    /**
     * Given an internal node of dynamic M-Index tree, this constructor identifies all logical
     *  nodes that share given physical bucket.
     * @param bucket physical bucket
     * @param internal internal M-Index tree node under which this multi bucket is located
     */
    public MultiBucket(LocalBucket bucket, VoronoiInternalCell internal) {
        this(bucket, internal.mIndex, (short) (internal.getLevel() + 1));
        for (Iterator<Entry<Short, VoronoiCell>> childNodes = internal.getChildNodes(); childNodes.hasNext(); ) {
            Entry<Short, VoronoiCell> childNode = childNodes.next();
            VoronoiCell leaf = childNode.getValue();
            if (leaf instanceof MIndexLeafCell && ((MIndexLeafCell) leaf).getStorage() == bucket) {
                nodes.put((MIndexLeafCell) leaf, childNode.getKey());
            }
        }
    }

    private MultiBucket(LocalBucket bucket, MetricIndex mIndex, short level) {
        this.bucket = bucket;
        this.mIndex = mIndex;
        this.level = level;
    }
    
    /**
     * Returns true if this multibucket is over the max occupation limit
     * @return true if this multibucket is over the max occupation limit
     */
    public boolean mustBeSplit() {
        return bucket.isSoftCapacityExceeded();
    }
    
    /**
     * Returns true if this is a multi-bucket that exceeds the low occupation at least twice.
     * @return true if this is a multi-bucket that exceeds the low occupation at least twice
     */
    private boolean couldBePossiblySplit() {
        return isMultiBucket() && bucket.getObjectCount() > mIndex.getBucketMinOccupation() * 2;
    }
    
    /**
     * True if this a multi-bucket (with at least 2 logical nodes storing data in this bucket).
     * @return if this a multi-bucket 
     */
    public boolean isMultiBucket() {
        return nodes.size() > 1;
    }

    /**
     * Prepares the possible partitioning of this multi-bucket into internal splitting structures, if possible.
     * Returns information about success.
     * @param force if true, then the split is planned in any case; if false, then the storage (multi-bucket) is split only if 
     *   none of the partitions would be under the minimum limit
     * @return true if succeeded 
     */
    public boolean prepareSplit(boolean force) {
        if (! couldBePossiblySplit() || nodes.size() <= 1) {
            return false;
        }
        nodesBucketMapping = new int [mIndex.getNumberOfPivots()];
        
        List<Collection<MIndexLeafCell>> partitioning = partition(force, mIndex.isAgile() ? 
                mIndex.getBucketMinOccupation() : (mIndex.getBucketDispatcher().getBucketSoftCapacity() / 2));
        if (partitioning.size() < 2) {
            return false;
        }
        for (int i = 1; i < partitioning.size(); i++) {
            for (MIndexLeafCell leaf : partitioning.get(i)) {
                nodesBucketMapping[nodes.get(leaf)] = i;
            }
        }
        
        return true;
    }

    /**
     * Creates a plan for partitioning of this multi-bucket.
     * @param force if true, then the split is planned in any case; if false, then the storage (multi-bucket) is split only if 
     *   none of the partitions would be under the minimum limit
     * @param min minimum occupation limit for each of the created partitions
     * @return the suggested partitioning of the leaf cells within this multi-bucket
     */
    private List<Collection<MIndexLeafCell>> partition(boolean force, long min) {
        Set<MIndexLeafCell> pool = new TreeSet<>(occInverseComparator);
        pool.addAll(nodes.keySet());

        ArrayList<Collection<MIndexLeafCell>> retVal = new ArrayList<>();
        retVal.add(0, pool);
        
        resultingBuckets = 1;
        int poolOccupation = bucket.getObjectCount();
        
        // iterate over the logical nodes starting from the largest and check if it can be isolated from the others
        while (poolOccupation >= min * 2 || (resultingBuckets == 1 && force)) {
            Iterator<MIndexLeafCell> iterator = pool.iterator();
            int newlyCreatedSize = 0;
            Collection<MIndexLeafCell> createdMultiBucket = new ArrayList<>();
            
            // find the smallest cell(s) that can be added to the largest in order fullfil the min limit
            MIndexLeafCell previousLeaf = null;
            while (newlyCreatedSize < min && iterator.hasNext()) {
                MIndexLeafCell leaf = iterator.next();
                // if we should try the smaller cell
                if (newlyCreatedSize != 0 && leaf.getObjectCount() + newlyCreatedSize >= min && iterator.hasNext()) {
                    previousLeaf = leaf;
                    continue;
                }
                if (previousLeaf != null && leaf.getObjectCount() + newlyCreatedSize < min) {
                    leaf = previousLeaf;
                }
                poolOccupation -= leaf.getObjectCount();
                newlyCreatedSize += leaf.getObjectCount();
                createdMultiBucket.add(leaf);
            }
            
            if (poolOccupation >= min || (resultingBuckets == 1 && force)) {
                retVal.add(resultingBuckets, createdMultiBucket);
                resultingBuckets ++;
                pool.removeAll(createdMultiBucket);
            }
        }
        
        return retVal;
    }
    
    
    /**
     * For given leaf node, this method returns the assigned number 
     * @param leaf
     * @return 
     */
    public int getNodeBucketAfterSplit(MIndexLeafCell leaf) {
        return nodesBucketMapping[nodes.get(leaf)];
    }
        
    /** Comparator of leaf nodes in decreasing order */
    private static final Comparator<VoronoiLeafCell> occInverseComparator = new Comparator<VoronoiLeafCell>() {
        @Override
        public int compare(VoronoiLeafCell t, VoronoiLeafCell t1) {
            int compareTo = ((Integer) t1.getObjectCount()).compareTo(t.getObjectCount());
            if (compareTo != 0) {
                return compareTo;
            }
            return ((Integer) t1.hashCode()).compareTo(t.hashCode());
        }
    };
    
    
    //****************** Matching ******************
    /**
     * Returns the number of the buckets to which the <code>object</code> belongs.
     *  Number 0 identifies the (multi-)bucket with objects that should stay and
     *  the rest 
     * 
     * @param object an object that is tested for partition
     * @return partition identification (index)
     */
    @Override
    public int match(LocalAbstractObject object) {
        MIndexPP objectPP = MetricIndexes.readMIndexPP(object);
        if (objectPP == null) {
            return -1;
        }
        //pp.setLevel(level, mIndex);
        return nodesBucketMapping[objectPP.getPivotIndex(level)];
    }

    /**
     * Returns the number of partitions of this policy.
     * @return the number of partitions of this policy
     */
    @Override
    public int getPartitionsCount() {
        return resultingBuckets;
    }

    @Override
    public int match(BallRegion region) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    public LocalBucket getBucket() {
        return this.bucket;
    }
    
    public Set<MIndexLeafCell> getNodes() {
        return this.nodes.keySet();
    }
}
