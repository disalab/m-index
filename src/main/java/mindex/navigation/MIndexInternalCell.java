/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.navigation;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import messif.algorithms.AlgorithmMethodException;
import messif.buckets.Bucket;
import messif.objects.LocalAbstractObject;
import mindex.MetricIndex;

/**
 * Encapsulates information about a super-cluster in the M-Index dynamic cluster hierarchy.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class MIndexInternalCell extends VoronoiInternalCell<LocalAbstractObject> {

    /** Class id for serialization */
    private static final long serialVersionUID = 41203L;
    
    /**
     * Create new object initializing cluster number, level and parentNode. The created node is empty - no child nodes are created!
     * @param mIndex M-Index logic
     * @param parentNode parent node of this node
     * @param pivotCombination the pivot combination corresponding to this cluster, e.g. [3,2,4] for cluster C_{3,2,4}
     * @throws AlgorithmMethodException if the cluster storage was not created successfully
     */
    public MIndexInternalCell(MetricIndex mIndex, VoronoiInternalCell parentNode, short[] pivotCombination) throws AlgorithmMethodException {
        super(mIndex, parentNode, pivotCombination);
    }


    // ************************   Sub-level manipulation  *********************************** //
    
    /**
     * Creates a specific child according to the type of this internal cell (data/nodata etc.)
     * @param internalNode if to crate an internal or leaf cell
     * @param newCombination PPP of the new node
     * @return newly created cell
     * @throws messif.algorithms.AlgorithmMethodException if the new node initiation goes wrong
     */
    @Override
    public VoronoiCell createSpecificChildCell(boolean internalNode, short[] newCombination) throws AlgorithmMethodException {
        return internalNode ? new MIndexInternalCell(mIndex, this, newCombination) :
                new MIndexLeafCell(mIndex, this, newCombination);        
    }    
    
    /**
     * Returns all leaf nodes (mutual siblings) that share given bucket as multi-bucket.
     * @param bucket a multi-bucket
     * @return all leaf nodes (mutual siblings) that share given bucket as multi-bucket
     */
    public Map<Short,MIndexLeafCell> getAllBucketNodes(Bucket bucket) {
        Map<Short,MIndexLeafCell> retVal = new HashMap<>();
//        for (Map.Entry<Short, VoronoiCell> leaf : ((Map<Short, VoronoiCell>)childNodes).entrySet()) {
        for (Iterator<Map.Entry<Short, VoronoiCell<LocalAbstractObject>>> it = getChildNodes(); it.hasNext();) {
            Map.Entry<Short, VoronoiCell<LocalAbstractObject>> child = it.next();
            if (child.getValue() instanceof MIndexLeafCell && ((MIndexLeafCell) child.getValue()).getStorage() == bucket) {
                retVal.put(child.getKey(), ((MIndexLeafCell) child.getValue()));
            }
        }
        return retVal;
    }
    
}
