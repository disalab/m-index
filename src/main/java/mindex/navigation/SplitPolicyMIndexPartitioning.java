/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.navigation;

import messif.buckets.split.SplitPolicy;
import messif.objects.BallRegion;
import messif.objects.LocalAbstractObject;
import mindex.MIndexKey;
import mindex.MIndexKeyInterval;
import mindex.MIndexKeyPPFull;
import mindex.MIndexPP;
import mindex.MIndexPPSimple;
import mindex.MetricIndexes;

/**
 * This class implements Voronoi-like split policy used in M-Index to split
 *   objects according to their pivot index on given level.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class SplitPolicyMIndexPartitioning extends SplitPolicy {

    /** Class id for serialization. */
    private static final long serialVersionUID = 12341201L;
    
    //****************** Attributes ******************

    /** Policy parameter <i>level</i> */
    @SplitPolicy.ParameterField("level")
    short level = 1;
    
    /** If true, the interval of covered keys is set for each created partition (up to {@link #numberOfPivots}) */
    @SplitPolicy.ParameterField("numberOfPivots")
    short numberOfPivots;

    /** If true, the covered intervals of MIndexKeys are calculated for each partition */
    boolean calculateIntervals = false;    

    /** Output parameter: covered key intervals for each partition */
    private MIndexKeyInterval [] intervals;    

    /** Occupation for each partition */
    private int [] occupations;
    
    
    //****************** Constructor ******************

    /** 
     * Creates a new instance of SplitPolicyBallPartitioning
     * @param numberOfPivots level of Voronoi partitioning (number of pivots)
     * @param level current level (1-based)
     */
    public SplitPolicyMIndexPartitioning(short level, short numberOfPivots) {
        this(level, false, numberOfPivots);
    }
    
    /** 
     * Creates a new instance of SplitPolicyBallPartitioning
     * @param numberOfPivots level of Voronoi partitioning (number of pivots)
     * @param level current level (1-based)
     * @param calculateIntervals if true, the interval of covered keys is set
     *   for each created partition (up to {@link #numberOfPivots})
     */
    public SplitPolicyMIndexPartitioning(short level, boolean calculateIntervals, short numberOfPivots) {
        setParameter("numberOfPivots", numberOfPivots);
        lockParameter("numberOfPivots");
        this.numberOfPivots = numberOfPivots;
        
        setParameter("level", level);
        lockParameter("level");
        this.level = level;
        
        this.calculateIntervals = calculateIntervals;
        
        if (calculateIntervals) {
            intervals = new MIndexKeyInterval[numberOfPivots];
        }
        occupations = new int [numberOfPivots];
    }
    
    //****************** Parameter quick setter/getters ******************

    /**
     * Given a partition number, this method returns the respective interval of keys
     *  that is covered by the partition created by this policy (capturing all touched objects).
     */
    public int getOccupation(int partitionID) {
        return occupations[partitionID];
    }
    
    /**
     * Given a partition number, this method returns the respective interval of keys
     *  that is covered by the partition created by this policy (capturing all touched objects).
     * @return null if the interval couldn't be determined (no data seen)
     */
    public MIndexKeyInterval getCoveredInterval(int partitionID) {
        if (! calculateIntervals) {
            return null;
        }
        return intervals[partitionID];
    }
    
    //****************** Matching ******************

    /**
     * Returns the index of partition to which the <code>object</code> belongs.
     * @param object an object that is tested for partition
     *
     * @return partition identification (index)
     */
    @Override
    public int match(LocalAbstractObject object) {
        MIndexPP objectPP = object.getDistanceFilter(MIndexKeyPPFull.class);
        if (objectPP == null) {
            objectPP = object.getDistanceFilter(MIndexPPSimple.class);
        }
        if (objectPP == null) {
            return -1;
        }
        int retVal = objectPP.getPivotIndex(level);
        try {
            occupations[retVal] ++;
        } catch (ArrayIndexOutOfBoundsException ex) {
            MetricIndexes.logger.severe("wrong PPP in object " + object.getLocatorURI() + " with PPP " + objectPP.toString() + " for level " + level);
        }
        
        if (calculateIntervals && (objectPP instanceof MIndexKey)) {
            objectPP.getClusterNumber(level);
            MIndexKey mIndexKey = (MIndexKey) objectPP;
            if (intervals[retVal] == null) {
                intervals[retVal] = new MIndexKeyInterval(mIndexKey, mIndexKey);
            } else {
                if (mIndexKey.compareTo(intervals[retVal].getFrom()) < 0) {
                    intervals[retVal].setFrom(mIndexKey);
                } else if (mIndexKey.compareTo(intervals[retVal].getTo()) > 0) {
                    intervals[retVal].setTo(mIndexKey);
                }
            }
        }
        return retVal;
    }

    /**
     * Returns the number of partitions of this policy.
     * @return the number of partitions of this policy
     */
    @Override
    public int getPartitionsCount() {
        return ((Short) getParameter("numberOfPivots")).intValue();
    }

    /**
     * NOT IMPLEMENTED YET!!!!
     *
     * Returns the index of partition to which the whole ball region belongs.
     * Returns -1 if not all objects from the specified ball region fall into just one partition
     * or if this policy cannot decide. In that case, the ball region must be searched one object by one
     * using the {@link #match(messif.objects.LocalAbstractObject) match(LocalAbstractObject)} method.
     *
     * @param region a ball region that is tested for the matching condition
     * @return the index of partition in which the ball region is contained completely or -1 if it is uncertain
     */
    @Override
    public int match(BallRegion region) {
        throw new UnsupportedOperationException("Not supported yet.");
    }


}
