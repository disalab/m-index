/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.navigation;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import mindex.MetricIndex;

/**
 * Abstract class as an ancestor of both internal node and leaf node of a cluster tree for a dynamic M-Index.
 * It covers one M-Index cluster or "super-cluster".
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public abstract class VoronoiCell<O extends LocalAbstractObject> implements Serializable {

    /** Class id for serialization */
    private static final long serialVersionUID = 41208L;
    
    /** Pointer to the parent node in the M-Index clusters hierarchy. */
    protected final VoronoiInternalCell parentNode;
    
    /** Link to the M-Index logic. */
    protected final MetricIndex mIndex;
    
    /**
     * Create new object initializing cluster number and level.
     * @param mIndex M-Index logic
     * @param parentNode parent node of this node
     */
    public VoronoiCell(MetricIndex mIndex, VoronoiInternalCell parentNode) {
        this.mIndex = mIndex;
        this.parentNode = parentNode;
    }   

    /**
     * Copy constructor.
     * @param copyFrom cell to copy the fields from (no cloning).
     */
    public VoronoiCell(VoronoiCell copyFrom) {
        this.mIndex = copyFrom.mIndex;
        this.parentNode = copyFrom.parentNode;
    }
    
    /**
     * Returns the parent node of this cell; null is returned for the root node
     * @return link to parent node
     */
    public VoronoiInternalCell getParentNode() {
        return parentNode;
    }    

    /**
     * Returns the M-Index configuration object.
     * @return M-Index configuration object
     */
    public MetricIndex getMIndex() {
        return mIndex;
    }
    
    // ***********************      Abstract methods    *********************** //
    
    /** Return the {@link #level} - the virtual tree root has level=0 */
    public abstract short getLevel();
    
    /**
     * Returns this cell PPP; this operation may be consuming; the PPP mustn't be modified from the outside.
     */
    public abstract short[] getPppForReading();

    /**
     * Returns this cell PPP; this operation may be consuming; the PPP mustn't be modified from the outside.
     */
    public abstract short[] getPppForReading(short thisIndex);
    
    /**
     * Iterator over all objects in the subtree specified by given node.
     * @return iterator over all objects in the subtree specified by given node.
     */
    public abstract AbstractObjectIterator<O> getAllObjects();

    /**
     * Return number of objects stored in the subtree specified by given node.
     * @return number of objects stored in the subtree specified by given node
     */
    public abstract int getObjectCount();
    public abstract int getObjectCountByObjects();

    /**
     * Given an M-Index pivot permutation, this method returns the child node corresponding to this permutation. If
     *  the leaf does not exist, it returns null or creates new (if the flag {@code createPath} is true).
     * @param ppp given pivot permutation (typically from query object)
     * @param createPath 
     * @return
     * @throws AlgorithmMethodException 
     */
    public abstract VoronoiLeafCell getResponsibleLeaf (short [] ppp, boolean createPath) throws AlgorithmMethodException;
    
    /**
     * This method should remove the data buckets (either in memory or on disk) from
     *  this cell or the whole subtree under this cell.
     * @throws AlgorithmMethodException if anything went wrong
     */
    public abstract void clearData() throws AlgorithmMethodException;

    /**
     * Calculates number of internal and leaf cells and volume (in bytes) of the data stored in the subtree of this
     *  node. The answer is to be stored in the three parameters.
     * @param intCellNumbers number of internal cells (output parameter)
     * @param branchingSums
     * @param leafCellNumber number of leaf cells (output parameters)
     * @param dataSizeBytes
     * @param leafLevelSum
     */
    public abstract void calculateTreeSize(Map<Short,AtomicInteger> intCellNumbers, Map<Short, AtomicLong> branchingSums, AtomicInteger leafCellNumber, AtomicLong dataSizeBytes, AtomicLong leafLevelSum);
    
    // **************             R/W locks         ********************* //
    
    public abstract void readLock();
    
    public abstract void readUnLock();
    
    public abstract void writeLock();
    
    public abstract void writeUnLock();
    
//    public String toString(short ... ppp) {
//        StringBuilder strBuf = new StringBuilder();
//        for (int i = 0; i < level; i++) {
//            strBuf.append("  ");
//        }
//        strBuf.append(toString(ppp, (short)1));
//        strBuf.append("level ").append(level).append(" pp ").append(Arrays.toString(ppp)).append(": ");
//        return strBuf.toString();
//    }
    
    public static void printBasicInfo(StringBuilder strBuf, short [] ppp, short ... lastLevels) {
        for (int i = 0; i < ppp.length + lastLevels.length; i++) {
            strBuf.append("  ");
        }
        strBuf.append("level ").append(ppp.length + lastLevels.length).append(" pp ").append(Arrays.toString(ppp));
        strBuf.delete(strBuf.length() - 1, strBuf.length());
        strBuf.append(Arrays.toString(lastLevels).substring(1));
    }
}
