/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex;

import gnu.trove.map.TObjectByteMap;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.TObjectLongMap;
import gnu.trove.map.TObjectShortMap;
import gnu.trove.map.hash.TObjectByteHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.map.hash.TObjectLongHashMap;
import gnu.trove.map.hash.TObjectShortHashMap;
import java.io.IOException;
import java.util.Properties;
import messif.utility.ExtendedProperties;
import messif.utility.ExtendedPropertiesException;

/**
 * This class extends the {@link ExtendedProperties} by identification of the
 * properties by enriched keys of class {@link Parameter}. The class is used for
 * M-Index configuration.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class MIndexProperties extends ExtendedProperties {

    /** Class serial ID for serialization */
    private static final long serialVersionUID = -4159810701357340958L;
    
    /** Cash of resolved values */
    private transient TObjectIntMap intCache;
    private transient TObjectShortMap shortCache;
    private transient TObjectLongMap longCache;
    private transient TObjectByteMap boolCache;
    
    /**
     * Creates these properties from any other properties, a (potential empty)
     * prefix, and (potentially null) default properties.
     *
     * @param properties any properties to create these enriched properties from
     * @param prefix prefix to restrict the passed properties
     */
    public MIndexProperties(Properties properties, String prefix) {
        super();
        initCaches();
        load(properties, prefix, null);
    }

    /**
     * Deserialization method - update the precomputed fields.
     * @param in stream
     * @throws java.io.IOException
     * @throws java.lang.ClassNotFoundException
     */
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        initCaches();
    }

    private void initCaches() {
        intCache = new TObjectIntHashMap();
        shortCache = new TObjectShortHashMap();
        longCache = new TObjectLongHashMap();
        boolCache = new TObjectByteHashMap();
    }
    
    // ***************************      Getters and setters for {@link Parameter}     ******************* //
    
    public int getIntProperty(Parameter key) throws ExtendedPropertiesException {
        if (key.getType() != Parameter.Type.INT) {
            throw new ExtendedPropertiesException("Property " + key.getKey() + " is not of type Integer");
        }
        int retVal = intCache.get(key);
        if (! intCache.containsKey(key)) {
            intCache.put(key, retVal = super.getIntProperty(key.getKey(), Integer.valueOf(key.getDefaultValue())));
        }
        return retVal;
    }

    public short getShortProperty(Parameter key) throws ExtendedPropertiesException {
        if (key.getType() != Parameter.Type.SHORT) {
            throw new ExtendedPropertiesException("Property " + key.getKey() + " is not of type Short");
        }
        short retVal = shortCache.get(key);
        if (! shortCache.containsKey(key)) {
            shortCache.put(key, retVal = (short) super.getIntProperty(key.getKey(), Integer.valueOf(key.getDefaultValue())));
        }
        return retVal;
    }

    public long getLongProperty(Parameter key) throws ExtendedPropertiesException {
        if (key.getType() != Parameter.Type.LONG) {
            throw new ExtendedPropertiesException("Property " + key.getKey() + " is not of type Long");
        }
        long retVal = longCache.get(key);
        if (! longCache.containsKey(key)) {
            longCache.put(key, retVal = Long.valueOf(getProperty(key.getKey(), key.getDefaultValue())));
        }
        return retVal;
    }

    public boolean getBoolProperty(Parameter key) throws ExtendedPropertiesException {
        if (key.getType() != Parameter.Type.BOOL) {
            throw new ExtendedPropertiesException("Property " + key.getKey() + " is not of type Boolean");
        }
        boolean retVal = boolCache.get(key) == 1;
        if (! boolCache.containsKey(key)) {
            boolCache.put(key, (retVal = super.getBoolProperty(key.getKey(), Boolean.valueOf(key.getDefaultValue()))) ? (byte) 1 : (byte)0);
        }
        return retVal;
    }
    
    public String getProperty(Parameter key) throws ExtendedPropertiesException {
        return super.getProperty(key.getKey(), key.getDefaultValue());
    }

    public void setProperty(Parameter key, String value) {
        super.setProperty(key.getKey(), value);
        switch (key.getType()) {
            case BOOL:
                boolCache.remove(key);
                break;
            case INT:
                intCache.remove(key);
                break;
            case LONG:
                longCache.remove(key);
                break;
            case SHORT:
                shortCache.remove(key);
                break;
        }
    }

    public void setProperty(Parameter key, boolean value) {
        if (key.getType() != Parameter.Type.BOOL) {
            throw new ExtendedPropertiesException("Property " + key.getKey() + " is not of type Boolean");
        }
        super.setProperty(key.getKey(), Boolean.toString(value));
        boolCache.put(key, value ? (byte) 1 : (byte)0);
    }

    public void setProperty(Parameter key, int value) {
        if (key.getType() != Parameter.Type.INT) {
            throw new ExtendedPropertiesException("Property " + key.getKey() + " is not of type Integer");
        }
        super.setProperty(key.getKey(), Integer.toString(value));
        intCache.put(key, value);
    }

    public void setProperty(Parameter key, short value) {
        if (key.getType() != Parameter.Type.SHORT) {
            throw new ExtendedPropertiesException("Property " + key.getKey() + " is not of type Short");
        }
        super.setProperty(key.getKey(), Short.toString(value));
        shortCache.put(key, value);
    }

    public void setProperty(Parameter key, long value) {
        if (key.getType() != Parameter.Type.LONG) {
            throw new ExtendedPropertiesException("Property " + key.getKey() + " is not of type long");
        }
        super.setProperty(key.getKey(), Long.toString(value));
        longCache.put(key, value);
    }
    
//    @Override
//    public Object setProperty(String key, String value) {
//        throw new IllegalStateException("The M-Index configuration cannot be modified via 'setProperty(String, String)' method");
//    }
}
