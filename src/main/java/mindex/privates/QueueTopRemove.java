/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.privates;

import java.util.NoSuchElementException;
import java.util.Queue;

/**
 * This interface extends the standard Queue interface by method that should
 *  efficiently remove N first elements.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public interface QueueTopRemove<E> extends Queue<E> {
    
    /**
     * Removes the specified number of top elements of this queue.
     * 
     * @param count number of objects to be removed
     * @throws NoSuchElementException if less then specified number of objects is stored
     */
    public void removeFirstN(int count) throws NoSuchElementException;
    
}
