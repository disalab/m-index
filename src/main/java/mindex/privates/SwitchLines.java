/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mindex.privates;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author xnovak8
 */
public class SwitchLines {

    /**
     * ColorLayoutType;messif.objects.impl.ObjectColorLayout;
     * ColorStructureType;* messif.objects.impl.ObjectShortVectorL1;
     * EdgeHistogramType;messif.objects.impl.ObjectVectorEdgecomp;
     * ScalableColorType;messif.objects.impl.ObjectIntVectorL1;
     * RegionShapeType;messif.objects.impl.ObjectXMRegionShape
     * 
     *         "ColorLayoutType", "ColorStructureType", "ScalableColorType", "EdgeHistogramType", "RegionShapeType"

     */
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try (BufferedReader fileReader = new BufferedReader(new FileReader("/share/datasets/imageCLEF/2014/profiset-random-249997-SAC.data"));
                BufferedWriter writer = new BufferedWriter(new FileWriter("/share/datasets/imageCLEF/2014/profiset-random-249997-SAC-right.data"))) {
            
            //String [] lines = new String[6];
            String line;
            while (fileReader.ready()) {                
                for (int i = 0; i < 3; i++) {
                    writer.write(fileReader.readLine());
                    writer.write('\n');
                }
                line = fileReader.readLine();
                
                writer.write(fileReader.readLine());
                writer.write('\n');
                writer.write(line);
                writer.write('\n');
                writer.write(fileReader.readLine());
                writer.write('\n');                
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SwitchLines.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SwitchLines.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
