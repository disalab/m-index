/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex;

/**
 * The abstract M-Index pivot permutation that contains the PP prefix and can
 *  convert it to cluster number on a given level.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public interface MIndexPP {

    /**
     * Get the pivot permutation (recommended to copy the internally stored permutation).
     * @return the full pivot permutation
     */
    public short[] getPivotPermutation();

    /**
     * Get the pivot permutation without copy the internally stored permutation.
     * @return the pivot permutation prefix
     */
    public short[] getPPPForReading();
    
    /**
     * Get the pivot index (0-based) at specified level (1-based). If the level
     * is smaller then 1 or larger than maxLevel then <code>-1</code> is returned.
     * @param level (1-based)
     * @return the pivot index at given level
     */
    public short getPivotIndex(short level);
    
    /** 
     * Create and return cluster number at a given level.
     * @param level (1-based)
     * @return the cluster number at a given level
     */
    public long getClusterNumber(short level);
    
    /**
     * Returns the maximal level of the M-Index (length of the PP prefix).
     * @return the maximal level of the M-Index (length of the PP prefix).
     */
    public short getMaxLevel();
    
}