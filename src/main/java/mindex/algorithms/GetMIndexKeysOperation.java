/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.algorithms;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import messif.objects.LocalAbstractObject;
//import messif.objects.impl.ObjectFeature;
import messif.operations.AbstractOperation.OperationName;
import messif.operations.data.BulkInsertOperation;
import mindex.MetricIndexes;

/**
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
@OperationName(value = "get M-Index keys for features")
@Deprecated
public class GetMIndexKeysOperation extends BulkInsertOperation {

    public GetMIndexKeysOperation(Collection<? extends LocalAbstractObject> insertedObjects) throws NoSuchElementException {
        super(insertedObjects);
    }

    
    
    /*@OperationConstructor(value = {"Iterator of objects to insert"})
     * public GetMIndexKeysOperation(Iterator<? extends ObjectFeature> insertedObjects) throws NoSuchElementException {
     * super(insertedObjects);
     * }
     * 
     * @OperationConstructor(value = {"Iterator of objects to insert, max count to read"})
     * public GetMIndexKeysOperation(Iterator<? extends ObjectFeature> objectsIterator, int count) throws NoSuchElementException {
     * super(objectsIterator, count);
     * }
     * 
     * @OperationConstructor(value = {"Iterator of objects to insert, mac count to read, flag if operation on 0 objects is permitted"})
     * public GetMIndexKeysOperation(Iterator<? extends ObjectFeature> objectsIterator, int count, boolean permitEmpty) throws NoSuchElementException {
     * super(objectsIterator, count, permitEmpty);
     * }*/
    
    
    /**
     * Prints the encapsulated objects to the given output stream.
     * @param out stream to print the objects to
     * @throws IOException if the printing went wrong
     */
    public void printAnswer(OutputStream out) throws IOException {
        for (LocalAbstractObject localAbstractObject : getInsertedObjects()) {
            localAbstractObject.write(out);
        }
    }
    
    /**
     * Given a GetMIndexKeysOperation and a file, this method appends to the file
     *  all objects' string representations encapsulated by this operation.
     * @param operation GetMIndexKeysOperation to read data from
     * @param fileName file name to append the info to
     */
    public static GetMIndexKeysOperation printAnswerToFile(GetMIndexKeysOperation operation, String fileName) {
        MetricIndexes.logger.log(Level.INFO, "printing answer to file: {0}", fileName);
        try (OutputStream out = new BufferedOutputStream(new FileOutputStream(fileName, true))) {
            operation.printAnswer(out);
        } catch (IOException ex) {
            MetricIndexes.logger.log(Level.WARNING, null, ex);
        }
        return operation;
    }

    @Override
    public String toString() {
        return new StringBuffer().append("GetMIndexKeysOperation on ").append(getNumberInsertedObjects()).append(" objects").toString();
    }
    
}
