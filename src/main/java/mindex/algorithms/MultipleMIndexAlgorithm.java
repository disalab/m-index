/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.algorithms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import messif.algorithms.Algorithm;
import messif.algorithms.AlgorithmMethodException;
import messif.algorithms.NavigationProcessor;
import messif.algorithms.impl.AbstractNavigationProcessor;
import messif.algorithms.impl.MultipleOverlaysAlgorithm;
import messif.operations.AbstractOperation;
import messif.operations.Approximate;
import messif.operations.RankingQueryOperation;
import messif.operations.RankingSingleQueryOperation;
import mindex.DuplicateDCFilter;
import mindex.MIndexObjectInitilizer;
import mindex.MetricIndex;

/**
 * Indexing algorithm that processes operation by passing them to encapsulated collection
 * of M-Index algorithms.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class MultipleMIndexAlgorithm extends MultipleOverlaysAlgorithm {
    
    /** Class id for serialization. */
    private static final long serialVersionUID = 201001L;

    /** Internal list of algorithms (the same as encapsulated ones from the constructor). */
    private transient MIndexAlgorithm[] algorithms;
    
    /** A simple lock for periodic algorithm serialization. */
    protected transient Lock serializingLock = new ReentrantLock();
    

    /**
     * Creates a new multi-algorithm overlay for the given array of algorithms.
     * @param algorithms the algorithms on which the operations are processed
     * @param cloneAsynchronousOperation the flag whether to clone the operation for asynchronous processing
     */
    public MultipleMIndexAlgorithm(Collection<? extends Algorithm> algorithms, boolean cloneAsynchronousOperation) {
        super(algorithms, cloneAsynchronousOperation);        
        setAlgorithms(algorithms);
    }

    /**
     * Creates a new multi-algorithm overlay for the given collection of algorithms.
     * @param algorithms the algorithms on which the operations are processed
     * @param cloneAsynchronousOperation the flag whether to clone the operation for asynchronous processing
     */
    @Algorithm.AlgorithmConstructor(description = "Constructor with created algorithms", arguments = {"array of running algorithms", "t/f if operation should be clonned before running"})
    public MultipleMIndexAlgorithm(Algorithm[] algorithms, boolean cloneAsynchronousOperation) {
        this(Arrays.asList(algorithms), cloneAsynchronousOperation);
    }

    /**
     * Initializes the internal array of encapsulated algorithms with the given collection.
     * @param algorithms the algorithms for the array
     */
    private void setAlgorithms(Collection<? extends Algorithm> algorithms) {
        this.algorithms = algorithms.toArray(new MIndexAlgorithm[algorithms.size()]);
    }

    /** Deserialize and set the transient fields. */
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        setAlgorithms(getAlgorithms());
        serializingLock = new ReentrantLock();        
    }

    @Override
    public void setOperationsThreadPool(ExecutorService operationsThreadPool) {
        super.setOperationsThreadPool(operationsThreadPool); //To change body of generated methods, choose Tools | Templates.
        for (Algorithm algorithm : algorithms) {
            if (algorithm.getOperationsThreadPool() == null) {
                algorithm.setOperationsThreadPool(operationsThreadPool);
            }
        }
    }
    
    @Override
    public <E extends AbstractOperation> List<Class<? extends E>> getSupportedOperations(Class<? extends E> subclassToSearch) {
        if (algorithms.length > 0) {
            return algorithms[0].getSupportedOperations(subclassToSearch);
        }
        return super.getSupportedOperations(subclassToSearch);
    }
    
    /**
     * Checks all the M-Index algorithms, if their dynamic cell tree was not modified and
     *  serializes the whole algorithm if it was changed. THis method is typically called
     *  periodically.
     * @param serializationFile file to serialize the whole algorithm into
     * @throws IOException if the serialization failed
     */
    public void checkModifiedAndStore(String serializationFile) throws IOException {
        serializingLock.lock();
        try {
            boolean anyModified = false;
            for (MIndexAlgorithm mIndexAlgorithm : algorithms) {
                anyModified = anyModified || mIndexAlgorithm.getmIndex().isDynamicTreeModified();
            }
            if (anyModified) {
                // Store algorithm to file
                this.storeToFile(serializationFile);
            }
        } finally {
            serializingLock.unlock();
        }
    }

    @Override
    protected void beforeStoreToFile(String filepath) {
        super.beforeStoreToFile(filepath);
        for (MIndexAlgorithm mIndexAlgorithm : algorithms) {
            mIndexAlgorithm.beforeStoreToFile(filepath);
        }
    }

    @Override
    protected void afterStoreToFile(String filepath, boolean successful) {
        super.afterStoreToFile(filepath, successful); 
        for (MIndexAlgorithm mIndexAlgorithm : algorithms) {
            mIndexAlgorithm.afterStoreToFile(filepath, successful);
        }
    }        
    
    /**
     * Sets the period of the automatic temporary-closeable checking of the buckets.
     * @param period the new checking period in milliseconds;
     *          zero value means disable the checking
     */
    public void autoCloseBuckets(long period) {
        for (MIndexAlgorithm mIndexAlgorithm : algorithms) {
            mIndexAlgorithm.autoCloseBuckets(period);
        }
    }

    public Collection<MIndexObjectInitilizer> getAllPPCalculators() {
        ArrayList<MIndexObjectInitilizer> pPCalculators = new ArrayList<>(algorithms.length);
        for (MIndexAlgorithm alg : algorithms) {
            pPCalculators.add(alg.getPPCalculator());
        }
        return pPCalculators;
    }
    
    protected MetricIndex getFirstMIndex() {
        return algorithms[0].getmIndex();
    }
    
    /** 
     * Clears (removes) all the data stored in all referenced M-Index algorithms. The structure of the 
     *  dynamic cell trees remains untouched.
     */
    public void clearData() throws AlgorithmMethodException {
        for (MIndexAlgorithm mIndexAlgorithm : algorithms) {
            mIndexAlgorithm.clearData();
        }
    }
    
    @Override
    public NavigationProcessor<? super AbstractOperation> getNavigationProcessor(AbstractOperation operation) {
        // Update answer flag in order to prevent duplicates in the merged answer
        if (operation instanceof RankingQueryOperation) {
            ((RankingQueryOperation)operation).setAnswerIgnoringDuplicates(true);
        }
        // divide the local search parameter by the number of encapsulated algorithms
        int approxLocalSearchParam = 0;
        if (operation instanceof Approximate) {
            approxLocalSearchParam = ((Approximate) operation).getLocalSearchParam();
            ((Approximate) operation).setLocalSearchParam(approxLocalSearchParam  / getAlgorithmsCount());
        }
        // prepare the check duplicate DC filter
         //   Variant 1
//        final TCustomHashSet duplicateDCFilter = ((operation instanceof RankingSingleQueryOperation) && (getFirstMIndex().isCheckDuplicateDC())) ?
//        final TCustomHashMap<Object, Object> duplicateDCFilter = TCollections.sync
//                ((operation instanceof RankingSingleQueryOperation) && (getFirstMIndex().isCheckDuplicateDC())) ?
//            new TCustomHashMap<>(new HashingStrategy<Object>() {
//                    @Override
//                    public int computeHashCode(Object object) {
//                        return object.hashCode();
//                    }
//
//                    @Override
//                    public boolean equals(Object o1, Object o2) {
//                        return o1 == o2;
//                    }
//                }, approxLocalSearchParam) : null;
//        final TObjectIntMap<Object> duplicateDCSet = ((operation instanceof RankingSingleQueryOperation) && (getFirstMIndex().isCheckDuplicateDC())) ?
//                TCollections.synchronizedMap(new TObjectIntHashMap(approxLocalSearchParam)) : null;

        // Variant 2
//        Set<Object> duplicateDCSet = ((operation instanceof RankingSingleQueryOperation) && (getFirstMIndex().isCheckDuplicateDC())) ?
//                new HashSet(approxLocalSearchParam) : null;
//        //final Set<Object> duplicateDCSetFinal = ((duplicateDCSet != null) && (getOperationsThreadPool() != null)) ? Collections.synchronizedSet(duplicateDCSet) : duplicateDCSet;
        
        // Variant 3
        Set<Object> duplicateDCSet = ((operation instanceof RankingSingleQueryOperation) && (getFirstMIndex().isCheckDuplicateDC())) ?
                new HashSet(approxLocalSearchParam) : null;
        final Set<Object> duplicateDCSetFinal = ((duplicateDCSet != null) && (getOperationsThreadPool() != null)) ? 
                Collections.newSetFromMap(new ConcurrentHashMap<Object, Boolean>(approxLocalSearchParam)) : duplicateDCSet;
        
        
        // clone the operation for both sequential and parallel processing  (operation, true, true)
        return new AbstractNavigationProcessor<AbstractOperation, Algorithm>(operation, true, true, getAlgorithms()) {
            @Override
            protected AbstractOperation processItem(AbstractOperation operation, Algorithm algorithm) throws AlgorithmMethodException {
                try {
                    // if the DC duplication filter is used then set
                    if (duplicateDCSetFinal != null) {
                        ((RankingSingleQueryOperation)operation).getQueryObject().chainFilter(new DuplicateDCFilter(null, duplicateDCSetFinal), true);
                    }
                    return algorithm.executeOperation(operation);
                } catch (NoSuchMethodException e) {
                    throw new AlgorithmMethodException(e);
                }
            }
        };
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(super.toString());
        int i = 1;
        for (MIndexAlgorithm mIndexAlgorithm : algorithms) {
            stringBuilder.append("Sub algorithm ").append(i++).append(": \n").append(mIndexAlgorithm.toString());
        }
        return stringBuilder.toString();
    }        
    
}
